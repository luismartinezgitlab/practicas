package com.athagon.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;



public class ConfiguracionProperties {

	protected static ConfiguracionProperties instance = null;
	
	protected ConfiguracionProperties() {
		obtenerConfig();
   }
   public static ConfiguracionProperties getInstance() {
	      if(instance == null) {
	         instance = new ConfiguracionProperties();
	      }
	      return instance;
	}
	private void obtenerConfig() {

		Properties prop = new Properties();
		InputStream input = null;
	 
		try {	 
			// input = new FileInputStream("classes/config.properties");
			Resource resource=new ClassPathResource("config.properties"); //FileSystemResource(“MyResource”)

			input = resource.getInputStream();
			
			// load a properties file
			prop.load(input);
	 
			cargarPropiedades(prop);
			
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}		 
	  }
	protected void cargarPropiedades(Properties prop) {
	}
}
