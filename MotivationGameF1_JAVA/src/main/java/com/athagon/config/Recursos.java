package com.athagon.config;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

public class Recursos {

	private static Recursos instance = null;

	HashMap<String, Resource> recursos;
	HashMap<String, String> recursosString;
	
	protected Recursos() {
		recursos = new HashMap<String, Resource>();
		recursosString = new HashMap<String, String>();
	}
	public static Recursos getInstance() {
	      if(instance == null) {
	         instance = new Recursos();
	      }
	      return instance;
	}
    public Resource getResource(String nombre) {
    	if (recursos.containsKey(nombre)) {
    		return recursos.get(nombre);
    	} else {
    		Resource resource=new ClassPathResource(nombre); //FileSystemResource(“MyResource”)
    		recursos.put(nombre, resource);
    		return resource;
    	}
    }
    public InputStream getResourceInputStream(String nombre) {
		InputStream input = null;
    	try {
    		input = getResource(nombre).getInputStream();
		} catch (IOException e) {
			e.printStackTrace();
		}
    	return input;
    }
    public String getStringInResource(String nombre) {
    	if (recursosString.containsKey(nombre)) {
    		return recursosString.get(nombre);
    	} else {
    		String textoEnRecurso = "";
    		StringBuilder sb=new StringBuilder();
    		try {
    	    	InputStream input = getResourceInputStream(nombre);

    			InputStreamReader is = new InputStreamReader(input);
    			BufferedReader br = new BufferedReader(is);
    			String read;
    			read = br.readLine();
    			
    			while(read != null) {
    			    //System.out.println(read);
    			    sb.append(read);
    			    read =br.readLine();			
    			}
        		textoEnRecurso = sb.toString();
        		recursosString.put(nombre, textoEnRecurso);
    		} catch (IOException e) {
    			System.out.println("No se consigue el recurso");
    			e.printStackTrace();
    		}		
    		return textoEnRecurso;    		
    	}
    }
}
