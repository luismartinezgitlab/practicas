package com.athagon.mail;

import com.athagon.config.Recursos;

public class LectorPlantilla {

	private String htmlPlantilla;
	private String plantillaAsunto;

	private String htmlMail;
	private String asunto;

	private String[] arrayCampos;
	
	public LectorPlantilla(String recursoHTML, String asuntoPlantilla, String[] arrayCampos) {
		super();
		this.htmlPlantilla = Recursos.getInstance().getStringInResource(recursoHTML);
		this.arrayCampos = arrayCampos;	
		this.plantillaAsunto = asuntoPlantilla;
		restaurarDesdePlantilla();
	}
	public void restaurarDesdePlantilla() {
		htmlMail = htmlPlantilla;
		asunto = plantillaAsunto;		
	}
	public void insertarValores(String[] arrayValores){
        for (int i = 0; i < arrayCampos.length; i++) {
        	htmlMail = htmlMail.replace("[" + arrayCampos[i] + "]", arrayValores[i]);
        	asunto = asunto.replace("[" + arrayCampos[i] + "]", arrayValores[i]);
        }
	}
	
	public void insertarArrayValores(String campo, String[] arrayValores) {
		if (arrayValores != null) {
	        for (int i = 0; i < arrayValores.length; i++) {
	        	htmlMail = htmlMail.replace("[" + campo + "_" + i + "]", arrayValores[i]);
	        	asunto = asunto.replace("[" + campo + "_" + i + "]", arrayValores[i]);
	        }
		}
	}
	public void insertarArrayValores(String campo, Object[] arrayValores) {
		if (arrayValores != null) {
	        for (int i = 0; i < arrayValores.length; i++) {
	        	htmlMail = htmlMail.replace("[" + campo + "_" + (i + 1) + "]", arrayValores[i].toString());
	        	asunto = asunto.replace("[" + campo + "_" + (i + 1) + "]", arrayValores[i].toString());
	        }
		}
	}
	public String getHTML() {
		return htmlMail;
	}
	public String getAsunto() {
		return asunto;
	}
}
