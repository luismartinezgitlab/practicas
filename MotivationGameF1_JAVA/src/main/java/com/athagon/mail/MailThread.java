package com.athagon.mail;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.athagon.mgf1.mail.GestorMailing;
import com.athagon.mgf1.repository.EnvioEmailRepository;
import com.athagon.mgf1.repository.PersonaRepository;

@Service("mailThread")
//@Scope("prototype")
public class MailThread  extends Thread{

	@Autowired
	private EnvioEmailRepository repoMails;
	@Autowired
	private PersonaRepository repoPersonas;
	
	@Value("${segundosEntreEnvios}")
	private int segundosEntreEnvios;
	@Value("${minutosEnvioEmailDesdeCreacion}")
	private int minutosEnvioEmailDesdeCreacion;
	@Value("${horasEnvioEmailSegundoIntento}")
	private int horasEnvioEmailSegundoIntento;
	@Value("${cantidadEmailsEnviadosPorVez}")
	private int cantidadEmailsEnviadosPorVez;
	
	@Override
	public void run() {

		if (segundosEntreEnvios < 5 )
			segundosEntreEnvios = 5;
		if (minutosEnvioEmailDesdeCreacion < 1 )
			minutosEnvioEmailDesdeCreacion = 1;
		
		GestorMailing gestorMail = new GestorMailing(repoMails, repoPersonas, minutosEnvioEmailDesdeCreacion, horasEnvioEmailSegundoIntento, cantidadEmailsEnviadosPorVez);
		while (true) {

			// System.out.println(">>> MAILING SYSTEM: " + getName() + " se está ejecutando");
			
			try {
				Thread.sleep(1000);
				
				Calendar fechaNuevoEnvio = new GregorianCalendar();
				fechaNuevoEnvio.add(Calendar.SECOND, segundosEntreEnvios);

				try {
					gestorMail.enviarEmailsPendientes();

				} catch (Exception e) {
					System.out.println(">>> MAILING SYSTEM: ERROR:");
					e.printStackTrace();
				}
				Calendar fechaActual = new GregorianCalendar();
				while (fechaActual.before(fechaNuevoEnvio)) {
					Thread.sleep(200);		
					fechaActual = new GregorianCalendar();
				}
				
			} catch (Exception e) {
				System.out.println(">>> MAILING SYSTEM: ERROR:");
				e.printStackTrace();
			}
		}
	}
}
