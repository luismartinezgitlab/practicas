package com.athagon.mail;

import java.util.GregorianCalendar;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class SMTPMailClient {

	public static final String DEB_LOG = "ATHAGON MAIL -> ";

	private String username;
    private String password;
	private Properties propiedadesSMTP;
	private  Authenticator authSMTP;
    private Session sessionSMTP;
	
    private MimeMessage mensajeMime;
    private Multipart multipart;
    
    private boolean ok;
	
    private static final boolean SMTP_DEBUG  = false;
    private static final String SMTP  = "smtp";
    private static final int 	SMTP_PUERTO_DEF  = 25;
    private static final String SMTP_AUTH_TRUE  = "true";
	
	public SMTPMailClient(String servidor, String usuario, String passwd) {
		
		inicializar(servidor, SMTP_PUERTO_DEF, usuario, passwd, SMTP_AUTH_TRUE);
	}
	public void inicializar(String servidor, int puerto, String usuario, String passwd, String auth) {

		propiedadesSMTP = new Properties();
		propiedadesSMTP.put("mail.transport.protocol", SMTP);
		propiedadesSMTP.put("mail.smtp.host", servidor);
		propiedadesSMTP.put("mail.smtp.port", puerto);		
		propiedadesSMTP.put("mail.smtp.auth", auth);
		
		this.username = usuario;
		this.password = passwd;
		if (auth == SMTP_AUTH_TRUE) {
			authSMTP = new SMTPAuthenticator();
		} else {
			authSMTP = null;
		}
        sessionSMTP = Session.getDefaultInstance(propiedadesSMTP, authSMTP);
        
        if (SMTP_DEBUG) 
        	sessionSMTP.setDebug(true);  
        
		log("Mail Client inicializado para host " + servidor);
	}
	public void crearMail(String asunto, String mensajeHTML, String remitente, boolean tipoHTML) {

		// log("Enviando correo " + asunto);
		
		ok = true;
		
		mensajeMime = new MimeMessage(sessionSMTP);
		
        multipart = new MimeMultipart("alternative");  
        
        if (tipoHTML)
        	addParteMail(mensajeHTML,  "text/html; charset=\"utf-8\"");
        else
        	addParteMail(mensajeHTML);
        
        if (ok) {
	        try {
				mensajeMime.setHeader("Content-Type","text/html; charset=\"utf-8\"");
		        mensajeMime.setContent(multipart,"text/alternative");		        
		        mensajeMime.setHeader("Content-Transfer-Encoding", "8bit");

		        mensajeMime.setSubject(asunto);
		        
		        mensajeMime.setFrom(new InternetAddress(remitente)); 
		        
			} catch (MessagingException e) {
				logError("No se puede crearMail de " + remitente + "\nAsunto: " + asunto);
				e.printStackTrace();
			}
        }
	}
	public void addCabecera(String nombre, String valor) {     
        try {
			mensajeMime.setHeader(nombre, valor);
		} catch (MessagingException e) {
			logError("No se puede addCabecera de " + nombre + ", valor: " + valor);
			e.printStackTrace();
		}		
	}
	public void addParteHTMLMail(String mensajeHTML) {
		
        addParteMail(mensajeHTML,  "text/html; charset=\"utf-8\"");     
	}
	public void addParteMail(String texto) {
		
        BodyPart parte = new MimeBodyPart();
        try {
			parte.setText(texto);
	        multipart.addBodyPart(parte);
	        
		} catch (MessagingException e) {
			ok = false;
			logError("No se puede addParteMail " + texto);
			e.printStackTrace();
		}        
	}
	public void addParteMail(String cuerpoMail, String tipo) {
		
        BodyPart parte = new MimeBodyPart();
        try {
			parte.setContent(cuerpoMail, tipo);
	        multipart.addBodyPart(parte);
	        
		} catch (MessagingException e) {
			ok = false;
			logError("No se puede addParteMail " + cuerpoMail);
			e.printStackTrace();
		}        		
	}
	private String dest = "";
	public boolean enviado() {
		return ok;
	}
	public void addDestinatarios(String[] emails) {
		for (int i = 0; i < emails.length; i++) {
			addDestinatario(emails[i], Message.RecipientType.TO);
		}
	}
	public void addDestinatariosCC(String[] emails) {
		for (int i = 0; i < emails.length; i++) {
			addDestinatario(emails[i], Message.RecipientType.CC);
		}
	}
	public void addDestinatariosBCC(String[] emails) {
		for (int i = 0; i < emails.length; i++) {
			addDestinatario(emails[i], Message.RecipientType.BCC);
		}
	}
	public void addDestinatario(String email) {
		addDestinatario(email, Message.RecipientType.TO);
		dest = email;
	}
	public void addDestinatarioCC(String email) {
		addDestinatario(email, Message.RecipientType.CC);
		dest = email;
	}
	public void addDestinatarioBCC(String email) {
		addDestinatario(email, Message.RecipientType.BCC);
		dest = email;
	}
	private void addDestinatario(String email, Message.RecipientType tipo) {
		if (ok) {
			try {
				mensajeMime.addRecipient(tipo, new InternetAddress(email));
			} catch (AddressException e) {
				ok = false;
				logError("AddressException - No se puede addDestinatario " + tipo.toString() + ": " + email);
				e.printStackTrace();
			} catch (MessagingException e) {
				ok = false;
				logError("MessagingException - No se puede addDestinatario " + tipo.toString() + ": " + email);
				e.printStackTrace();
			}
		}
	}

	public void enviarMail() {
		
		if (ok) {
	        Transport transport = null;

			try {
				transport = sessionSMTP.getTransport();
			} catch (NoSuchProviderException e) {
				ok = false;
				logError("NoSuchProviderException - No se puede enviarMail ");
				e.printStackTrace();
			}
			if (ok  && transport != null) {
		        try {
					transport.connect();
			        transport.sendMessage(mensajeMime, mensajeMime.getAllRecipients());
			        transport.close();
				} catch (MessagingException e) {
					ok = false;
					logError("MessagingException - No se puede enviar el mail " + mensajeMime.toString());
					e.printStackTrace();
				}
			}
			if (ok) {
				log("Correo/s enviado correctamente a " + dest + "...");
			}
		} else {
			logError("No se puede enviarMail porque el email no ha sido creado");
		}
	}
	public static void log(String texto) {
		// System.out.println("__________________________________________________");
		System.out.println(DEB_LOG +"[" + (new GregorianCalendar().getTime().toString()) +"]: " + texto);		
	}
	public static void logError(String texto) {
		log("ERROR: " + texto);		
	}
	public static String combinarNombreYmail(String nombre, String apellidos, String email) {
		return nombre + " " + apellidos + " <" + email + ">";
	}
    private class SMTPAuthenticator extends javax.mail.Authenticator {

        public PasswordAuthentication getPasswordAuthentication() {
           return new PasswordAuthentication(username, password);
        }
    }
}
