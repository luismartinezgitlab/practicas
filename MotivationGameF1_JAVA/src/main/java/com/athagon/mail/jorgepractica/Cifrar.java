package com.athagon.mail.jorgepractica;
import java.util.StringTokenizer;

import org.apache.tomcat.util.codec.binary.Base64;
import org.json.JSONObject;

public class Cifrar {

    static byte[] encoded;
	static byte[] decoded;
	
	public static String codificar(String usuario, String idEncuesta){
		//crear el json con usuario y idencuesta, y codificarlo toString
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("Usuario", usuario);
		jsonObject.put("Encuesta", idEncuesta);		
		encoded = Base64.encodeBase64(jsonObject.toString().getBytes());
		return new String(encoded);
	}
	
	public static String decodificar(String cadena){
		decoded = Base64.decodeBase64(cadena.getBytes());
		return new String(decoded);
	}
	
	public static String[] desmembrar(String cadena){
		//No se usa por que cambiamos a JSON de Usuario y Encuesta
		StringTokenizer tokens = new StringTokenizer(cadena,":");
		String datos[] = new String[2];
		while(tokens.hasMoreElements())
		{
			datos[0] = (String) tokens.nextElement();
			datos[1] = (String) tokens.nextElement();
		}
		return datos;
	}
	
}
