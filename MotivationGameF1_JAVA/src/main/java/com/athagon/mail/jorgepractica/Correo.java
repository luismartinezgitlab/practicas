package com.athagon.mail.jorgepractica;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


public class Correo {

	static Properties mailServerProperties;
	static Session getMailSession;
	static MimeMessage generateMailMessage;
		
	public Correo() {}
	
	public static void cargar()
	{
		mailServerProperties = System.getProperties();
		mailServerProperties.put("mail.smtp.port", "587");
		mailServerProperties.put("mail.smtp.auth", "true");
		mailServerProperties.put("mail.smtp.starttls.enable", "true");
	}	

	public static void enviarCorreo(String correo, String password) throws MessagingException
	{
		Transport transport = getMailSession.getTransport("smtp");
		transport.connect("smtp.gmail.com", correo, password);
		transport.sendMessage(generateMailMessage, generateMailMessage.getAllRecipients());
		transport.close();
	}
	
	public static void crearCorreo(String destino, String asunto, String mensaje) throws AddressException, MessagingException
	{
		getMailSession = Session.getDefaultInstance(mailServerProperties, null);
		generateMailMessage = new MimeMessage(getMailSession);
		generateMailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(destino));
		generateMailMessage.setSubject(asunto);
		String emailBody = mensaje; //Se le puede a�adir HTML
		generateMailMessage.setContent(emailBody, "text/html");
	}
	
	public static void crearCorreoCC(String destino, String destinoCC, String asunto, String mensaje) throws AddressException, MessagingException
	{
		getMailSession = Session.getDefaultInstance(mailServerProperties, null);
		generateMailMessage = new MimeMessage(getMailSession);
		generateMailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(destino));
		generateMailMessage.addRecipient(Message.RecipientType.CC, new InternetAddress(destinoCC));
		generateMailMessage.setSubject(asunto);
		String emailBody = mensaje; //Se le puede a�adir HTML
		generateMailMessage.setContent(emailBody, "text/html");
	}
	
	
	
}
