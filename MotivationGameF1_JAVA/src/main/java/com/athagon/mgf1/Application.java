package com.athagon.mgf1;


import java.util.Arrays;

import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.athagon.mail.MailThread;

@Configuration
@ComponentScan(basePackages="com.athagon.mgf1")
@SpringBootApplication
public class Application {

  public static void main(String[] args) {
	ApplicationContext ctx = SpringApplication.run(Application.class, args);
	// ApplicationContext ctxConfig = new AnnotationConfigApplicationContext(AppConfig.class);
	System.out.println("Let's inspect the beans provided by Spring Boot:");

	logo();
	// mostrarBeans (ctx);
	// mostrarBeans (ctxConfig);
	
	MailThread hiloMailing = new MailThread();
	ctx.getAutowireCapableBeanFactory().autowireBeanProperties(
			hiloMailing,
		    AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);
	// ctx.getAutowireCapableBeanFactory().autowireBean(hiloMailing);
	// hiloMailing.setName("Athagon - Mailing system thread");
	hiloMailing.start();
  }
  /*
  @Bean public EmbeddedServletContainerFactory servletContainer() { 
	  TomcatEmbeddedServletContainerFactory factory = new TomcatEmbeddedServletContainerFactory(); 
	  factory.addConnectorCustomizers(
			  new TomcatConnectorCustomizer() { 
				  @Override public void customize(Connector connector) { 
					  connector.setPort(8443); 
					  connector.setSecure(true); 
					  connector.setScheme("https");
					  connector.setAttribute("keyAlias", "tomcat");
					  connector.setAttribute("keystorePass", "changeit"); 
					  try { 
						  connector.setAttribute("keystoreFile", ResourceUtils.getFile("src/ssl/tomcat.keystore").getAbsolutePath()); 
						  } catch (FileNotFoundException e) { throw new IllegalStateException("Cannot load keystore", e); } connector.setAttribute("clientAuth", "false"); connector.setAttribute("sslProtocol", "TLS"); connector.setAttribute("SSLEnabled", true); } }); factory.setSessionTimeout(10, TimeUnit.MINUTES); return factory; 
	  }
*/
	  
	  /*
 @Bean
  public EmbeddedServletContainerFactory servletContainer() {
    TomcatEmbeddedServletContainerFactory tomcat = new TomcatEmbeddedServletContainerFactory() {
        @Override
        protected void postProcessContext(Context context) {
          SecurityConstraint securityConstraint = new SecurityConstraint();
          securityConstraint.setUserConstraint("CONFIDENTIAL");
          SecurityCollection collection = new SecurityCollection();
          collection.addPattern("/*");
          securityConstraint.addCollection(collection);
          context.addConstraint(securityConstraint);
        }
      };
    
    tomcat.addAdditionalTomcatConnectors(initiateHttpConnector());
    return tomcat;
  }
  
  private Connector initiateHttpConnector() {
    Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
    connector.setScheme("http");
    connector.setPort(8080);
    connector.setSecure(false);
    connector.setRedirectPort(8443);
    
    return connector;
  }
  private Connector createStandardConnector() {
		Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
	    connector.setScheme("http");
	    connector.setPort(8080);
	    connector.setSecure(false);
		return connector;
	}
*/
  static void logo() {

	    System.out.println("********************************************");
	    System.out.println("**                                        **");
	    System.out.println("**   A T H A G O N - Motivation Game F1   **");
	    System.out.println("**                                        **");
	    System.out.println("********************************************");
  }
  static void mostrarBeans( ApplicationContext ctx) {
		System.out.println("____________________________________________________");
		System.out.println("");
		String[] beanNames = ctx.getBeanDefinitionNames();
		Arrays.sort(beanNames);
		for (String beanName : beanNames) {
			System.out.println(beanName);
		}
		System.out.println("____________________________________________________");
	  
  }
}
