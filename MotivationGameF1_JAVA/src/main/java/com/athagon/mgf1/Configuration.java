package com.athagon.mgf1;

import java.util.Properties;

import com.athagon.config.ConfiguracionProperties;

public class Configuration extends ConfiguracionProperties {

	private String emailSoporte;
	
	private float coberturaMinima;
	private float coberturaMaxima;
	private float puntosPorUnidadCobertura;
	
	private boolean codigosSeguridadActivos;
	
	private String asuntoMailActivacion;
	private String fichMailActivacion;
	private String asuntoMailCodigos;
	private String fichMailCodigos;
	private String asuntoMailPreavisoInicio;
	private String fichMailPreavisoInicio;
	private String asuntoMailSolicitudPago;
	private String fichMailSolicitudPago;
	private String asuntoMailResultadosProv;
	private String fichMailResultadosProv;
	private String asuntoMailResultadosDef;
	private String fichMailResultadosDef;
	
	private String asuntoMailSolicitudLogro;
	private String fichMailSolicitudLogro;
	private String asuntoMailResultadosCarrera;
	private String fichMailResultadosCarrera;

	public static Configuration getInstance() {
		      if(instance == null) {
		         instance = new Configuration();
		      }
		      return (Configuration) instance;
		}
	@Override 
	protected void cargarPropiedades(Properties prop) {

		// get the property value and print it out
		emailSoporte = prop.getProperty("emailSoporte");

		asuntoMailActivacion = 			prop.getProperty("asuntoMailActivacion");
		fichMailActivacion = 			prop.getProperty("fichMailActivacion");
		asuntoMailCodigos = 			prop.getProperty("asuntoMailCodigos");
		fichMailCodigos = 				prop.getProperty("fichMailCodigos");
		asuntoMailPreavisoInicio = 		prop.getProperty("asuntoMailPreavisoInicio");
		fichMailPreavisoInicio = 		prop.getProperty("fichMailPreavisoInicio");
		asuntoMailSolicitudPago = 		prop.getProperty("asuntoMailSolicitudPago");
		fichMailSolicitudPago = 		prop.getProperty("fichMailSolicitudPago");
		asuntoMailResultadosProv = 		prop.getProperty("asuntoMailResultadosProv");
		fichMailResultadosProv = 		prop.getProperty("fichMailResultadosProv");
		asuntoMailResultadosDef = 		prop.getProperty("asuntoMailResultadosDef");
		fichMailResultadosDef = 		prop.getProperty("fichMailResultadosDef");
		
		asuntoMailSolicitudLogro = 		prop.getProperty("asuntoMailSolicitudLogro");
		fichMailSolicitudLogro = 		prop.getProperty("fichMailSolicitudLogro");
		asuntoMailResultadosCarrera = 	prop.getProperty("asuntoMailResultadosCarrera");
		fichMailResultadosCarrera = 	prop.getProperty("fichMailResultadosCarrera");
		
		coberturaMinima = 			Float.parseFloat(prop.getProperty("coberturaMinima"));
		coberturaMaxima = 			Float.parseFloat(prop.getProperty("coberturaMaxima"));
		puntosPorUnidadCobertura = 	Float.parseFloat(prop.getProperty("puntosPorUnidadCobertura"));

		codigosSeguridadActivos = 	Boolean.parseBoolean(prop.getProperty("codigosSeguridadActivos"));
	}
	public String getAsuntoMailActivacion() {
		return asuntoMailActivacion;
	}
	public String getFichMailActivacion() {
		return fichMailActivacion;
	}
	public String getAsuntoMailCodigos() {
		return asuntoMailCodigos;
	}
	public String getFichMailCodigos() {
		return fichMailCodigos;
	}
	public String getAsuntoMailPreavisoInicio() {
		return asuntoMailPreavisoInicio;
	}
	public String getFichMailPreavisoInicio() {
		return fichMailPreavisoInicio;
	}
	public String getAsuntoMailSolicitudPago() {
		return asuntoMailSolicitudPago;
	}
	public String getFichMailSolicitudPago() {
		return fichMailSolicitudPago;
	}
	public String getAsuntoMailResultadosProv() {
		return asuntoMailResultadosProv;
	}
	public String getFichMailResultadosProv() {
		return fichMailResultadosProv;
	}
	public String getAsuntoMailResultadosDef() {
		return asuntoMailResultadosDef;
	}
	public String getFichMailResultadosDef() {
		return fichMailResultadosDef;
	}
	public String getAsuntoMailSolicitudLogro() {
		return asuntoMailSolicitudLogro;
	}
	public String getFichMailSolicitudLogro() {
		return fichMailSolicitudLogro;
	}
	public String getAsuntoMailResultadosCarrera() {
		return asuntoMailResultadosCarrera;
	}
	public String getFichMailResultadosCarrera() {
		return fichMailResultadosCarrera;
	}
	public float getCoberturaMinima() {
		return coberturaMinima;
	}
	public float getCoberturaMaxima() {
		return coberturaMaxima;
	}
	public float getPuntosPorUnidadCobertura() {
		return puntosPorUnidadCobertura;
	}
	public boolean getCodigosSeguridadActivos() {
		return codigosSeguridadActivos;
	}
	
}
