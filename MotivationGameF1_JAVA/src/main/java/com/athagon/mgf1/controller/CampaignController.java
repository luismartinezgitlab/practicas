package com.athagon.mgf1.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.athagon.mgf1.model.Campaign;
import com.athagon.mgf1.repository.CampgaignRepository;

/**
 * @author �lvaro del Pozo Cuenca
 *
 */
@RestController
@RequestMapping("/items")
public class CampaignController {
	
  @Autowired
  private CampgaignRepository repo;
  
  @RequestMapping(method = RequestMethod.GET)
  public List<Campaign> findCampaigns() {
	  System.out.println("Obteniendo Campaigns");
    return repo.findAll();
  }
  
  @RequestMapping(method = RequestMethod.POST)
  public Campaign addCampaign(@RequestBody Campaign campaign) {
    campaign.setId(null);
    System.out.println("Agregando Campana [" + campaign.getDescription() + "]");
    return repo.saveAndFlush(campaign);
  }
  
  @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
  public Campaign updateCampaign(@RequestBody Campaign updatedCampaign, @PathVariable Integer id) {
    updatedCampaign.setId(id);
    return repo.saveAndFlush(updatedCampaign);
  }
  
  @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
  public void deleteCampaign(@PathVariable Integer id) {
    repo.delete(id);
  }
}
