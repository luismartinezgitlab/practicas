package com.athagon.mgf1.controller;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.athagon.mgf1.model.Campeonato;
import com.athagon.mgf1.model.Carrera;
import com.athagon.mgf1.model.Persona;
import com.athagon.mgf1.repository.CampeonatoRepository;
import com.athagon.mgf1.repository.CarreraRepository;

// import com.athagon.mgf1.repository.ComentarioRepository;

/*
 * COMPROBACION CAPTCHA:
 * When your users submit the form where you integrated reCAPTCHA, you'll get as part of the payload a string with the name "g-recaptcha-response". In order to check whether Google has verified that user, send a POST request with these parameters:
	URL: https://www.google.com/recaptcha/api/siteverify
	secret(required)	6LdBwQYTAAAAAIVtk4ZqQZaemKxCNHKsi_VqtwDi
	response(required)	El valor de "g-recaptcha-response".
	remoteip	The end user's ip address.
 */
@RestController
@RequestMapping("/campeonatos")
public class CampeonatoController {

  @Autowired
  private CampeonatoRepository repo;
  @Autowired 
  private CarreraRepository carreraRepo;  
  
  private final Logger log = LoggerFactory.getLogger(this.getClass());	
  
  @RequestMapping(value = "/{id}", method = RequestMethod.GET)
  public Campeonato findCampeonato(@PathVariable Integer id) {
	  Campeonato campeonato = repo.findOne(id);
	  if (campeonato != null) {
		  log.info("* M1 * Obteniendo campeonato " + campeonato.toString());
	  } else {
		  log.info("* M1 * El campeonato con ID " + id + " no existe");
	  }
    return campeonato;
  }
  @RequestMapping(method = RequestMethod.POST)
  public Campeonato addCampeonato(@RequestBody Campeonato campeonato) {
	  if (campeonato.getNombre() != null
			  && campeonato.getNombre().length() > 1) {
		  log.info("* M1 * Alta campeonato con nombre [" + campeonato.getNombre() + "] " );
		  campeonato.setId(null);
		  //campeonato.setNumCarreras((campeonato.getFechaFin() - campeonato.getFechaFin() ) );
		  
		  // La fecha de inicio debe ser LUNES
		  Calendar fechaInicio  = Calendar.getInstance();
		  fechaInicio.setTime(campeonato.getFechaInicio());
		  
		  switch (fechaInicio.get(Calendar.DAY_OF_WEEK)) {
		  case Calendar.TUESDAY: 	fechaInicio.add(Calendar.DATE, -1); break;
		  case Calendar.WEDNESDAY:	fechaInicio.add(Calendar.DATE, -2); break;
		  case Calendar.THURSDAY:	fechaInicio.add(Calendar.DATE, -3); break;
		  case Calendar.FRIDAY:		fechaInicio.add(Calendar.DATE, -4); break;
		  case Calendar.SATURDAY:	fechaInicio.add(Calendar.DATE, -5); break;
		  case Calendar.SUNDAY:		fechaInicio.add(Calendar.DATE, -6); break;
		  };
		  campeonato.setFechaInicio(fechaInicio.getTime());
		  
		  // La fecha de fin debe ser DOMINGO
		  Calendar fechaFin  = Calendar.getInstance();
		  fechaFin.setTime(campeonato.getFechaFin());
		  
		  switch (fechaFin.get(Calendar.DAY_OF_WEEK)) {
		  case Calendar.MONDAY:		fechaFin.add(Calendar.DATE, 6); break;
		  case Calendar.TUESDAY: 	fechaFin.add(Calendar.DATE, 5); break;
		  case Calendar.WEDNESDAY:	fechaFin.add(Calendar.DATE, 4); break;
		  case Calendar.THURSDAY:	fechaFin.add(Calendar.DATE, 3); break;
		  case Calendar.FRIDAY:		fechaFin.add(Calendar.DATE, 2); break;
		  case Calendar.SATURDAY:	fechaFin.add(Calendar.DATE, 1); break;
		  };
		  campeonato.setFechaFin(fechaFin.getTime());
		  
		  Campeonato nuevo = repo.saveAndFlush(campeonato);
		  String categoriaAcum =  "M1-" + nuevo.getId() + "-" + nuevo.getNombre();
		  categoriaAcum = categoriaAcum.substring(0, categoriaAcum.length() < 40 ? categoriaAcum.length() : 40);
		  nuevo.setCategoriaAcumbamail(categoriaAcum);
		  repo.saveAndFlush(nuevo);
		  return nuevo;
	  } else {
		  log.info("* M1 * No se puede dar de alta el campeonato porque no tiene nombre");
		  return null;
	  }
  }
  @RequestMapping(value = "/carrera", method = RequestMethod.GET)
  public Carrera findCarreraActual(@RequestBody Campeonato campeonatoActual) {
	  Carrera carreraActual = carreraRepo.findCarreraActual(campeonatoActual.getId());
	  if (carreraActual != null) {
		  log.info("* M1 * Obteniendo carrera actual " + carreraActual.toString());
	  } else {
		  //TODO: �Y si la carrera no ha empezado?
		  log.info("* M1 * La carrera actual del " + campeonatoActual.toString() + " no existe. Obteniendo ultima carrera");
		  carreraActual = carreraRepo.findUltimaCarrera(campeonatoActual.getId());
		  if (carreraActual == null) {
			  log.info("* M1 * No hay carreras para el campeonato " + campeonatoActual.toString() );
		  }
	  }
    return carreraActual;
  }
  @RequestMapping(value = "/carrera/anterior", method = RequestMethod.GET)
  public Carrera findCarreraAnterior(@RequestBody Campeonato campeonatoActual) {

	  Carrera carreraAnterior = carreraRepo.findCarreraAnterior(campeonatoActual.getId());
	  if (carreraAnterior != null) {
		  log.info("* M1 * Obteniendo carrera anterior " + carreraAnterior.toString());
	  } else {
		  //TODO: �Y si la carrera no ha empezado? ESTO CALCULARLO BIEN
		  log.info("* M1 * La carrera anterior del " + campeonatoActual.toString() + " no existe. Obteniendo ultima carrera");
		  carreraAnterior = carreraRepo.findUltimaCarrera(campeonatoActual.getId());
		  if (carreraAnterior == null) {
			  log.info("* M1 * No hay carreras para el campeonato " + campeonatoActual.toString() );
		  }
	  }
    return carreraAnterior;
  }
  @RequestMapping(value = "/porparticip", method = RequestMethod.GET)
  public HashMap<Integer, Campeonato> findCampeonatosPorParticipante(@RequestBody Persona participante) {
	  HashMap<Integer, Campeonato> campeonatosAgregados = new HashMap<Integer, Campeonato>();
	  List<Campeonato> campeonatos = repo.findByParticipante(participante.getId()); 
	 log.info("* M1 * Obteniendo Campeonatos del Participante " + participante.toString() + ": " + campeonatos.size() + " campeonatos");
	  for (int i = 0; i < campeonatos.size(); i++) {
		  
		  campeonatosAgregados.put(i, campeonatos.get(i));
	  }
     return campeonatosAgregados;
  }
  /*
  @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
  public Campeonato updateCampeonato(@RequestBody Campeonato updatedCampeonato, @PathVariable Integer id) {
	  updatedCampeonato.setId(id);
    return repo.saveAndFlush(updatedCampeonato);
  }
  
  @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
  public void deleteCampeonato(@PathVariable Integer id) {
    repo.delete(id);
  }*/
}
