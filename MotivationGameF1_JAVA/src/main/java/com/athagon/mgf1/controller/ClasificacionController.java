package com.athagon.mgf1.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.athagon.mgf1.logica.SistemaClasificacionCampeonato;
import com.athagon.mgf1.logica.SistemaClasificacionCarrera;
import com.athagon.mgf1.model.Campeonato;
import com.athagon.mgf1.model.Carrera;
import com.athagon.mgf1.model.ClasificacionCarrera;
import com.athagon.mgf1.repository.CarreraRepository;
import com.athagon.mgf1.repository.ClasificacionRepository;
import com.athagon.mgf1.repository.ParticipacionRepository;
import com.athagon.mgf1.repository.PuntosPorPosicionRepository;

/**
 * @author German Caballero Rodriguez
 *
 */
@RestController
@RequestMapping("/clasificacion")
public class ClasificacionController {

	  @Autowired 
	  private CarreraRepository carreraRepo;
	  @Autowired 
	  private ClasificacionRepository clasificacionRepo;
	  @Autowired
	  private PuntosPorPosicionRepository puntosPPosRepo;
	  @Autowired
	  private ParticipacionRepository participacionRepo;
	  
	  private final Logger log = LoggerFactory.getLogger(this.getClass());	
	  
	  private static SistemaClasificacionCarrera sistClasifCarr;  
	  private static SistemaClasificacionCampeonato sistClasifCamp;  
	  
	  private void inicializarSistemaClasificacion() {
		  
		  if (sistClasifCarr == null) {

			  sistClasifCarr = new SistemaClasificacionCarrera(puntosPPosRepo, participacionRepo);
			  sistClasifCamp = new SistemaClasificacionCampeonato(puntosPPosRepo, participacionRepo);
		  }
	  }
	  @RequestMapping(value = "/carrera", method = RequestMethod.GET)
	  public  List<ClasificacionCarrera> findClasificacionCarrera(@RequestBody Carrera carreraActual) {

		  inicializarSistemaClasificacion();
		  List<ClasificacionCarrera>  clasifCarrByCarr = clasificacionRepo.findClasificacionCarerraByCarrera(carreraActual.getId());
		  log.info("* M1 * Obteniendo clasificacion de carrera " + carreraActual.toString());
		  sistClasifCarr.calcularPosicionesCarrera(clasifCarrByCarr);
		  return clasifCarrByCarr;
	  }  
	  @RequestMapping(value = "/campeonato/total", method = RequestMethod.GET)
	  public  List<ClasificacionCarrera> findClasificacionCampeonato(@RequestBody Campeonato campeonatoActual) {

		  inicializarSistemaClasificacion();

		  Carrera carreraActual = findCarreraActual(campeonatoActual);
		  
		  List<ClasificacionCarrera>  clasifCarrByCamp =  clasificacionRepo.findClasificacionCarerrasByCampeonato(campeonatoActual.getId(), carreraActual.getId());
		  log.info("* M1 * Obteniendo clasificacion final de campenato " + campeonatoActual.toString());
		  sistClasifCamp.calcularPosicionesCampeonato(clasifCarrByCamp);
		  List<ClasificacionCarrera> clasifCampTotal = new ArrayList<ClasificacionCarrera>();
		  for (ClasificacionCarrera clasifCarrera : clasifCarrByCamp) {
			  if (clasifCarrera.getCarrera() == carreraActual.getId()) {
				  clasifCampTotal.add(clasifCarrera);
			  }
		  }
		  return clasifCampTotal;
	  } 
	  @RequestMapping(value = "/campeonato/porcarrera", method = RequestMethod.GET)
	  public  List<ClasificacionCarrera> findClasificacionCampeonatoPorCarrera(@RequestBody Campeonato campeonatoActual) {

		  inicializarSistemaClasificacion();
		  Carrera carreraActual = findCarreraActual(campeonatoActual);
		  List<ClasificacionCarrera>  clasifCarrByCamp =  clasificacionRepo.findClasificacionCarerrasByCampeonato(campeonatoActual.getId(), carreraActual.getId());
		  log.info("* M1 * Obteniendo clasificacion de campenato por carrera " + campeonatoActual.toString());
		  sistClasifCamp.calcularPosicionesCampeonato(clasifCarrByCamp);
		  return clasifCarrByCamp;
	  }
	  /*
	   * Buscar carrerra actual por campeonato
	   */
	  private  Carrera findCarreraActual(Campeonato campeonatoActual) {
		  Carrera carreraActual = carreraRepo.findCarreraActual(campeonatoActual.getId());
		  if (carreraActual != null) {
			  log.info("* M1 * Obteniendo carrera actual " + carreraActual.toString());
		  } else {
			  log.info("* M1 * La carrera actual del " + campeonatoActual.toString() + " no existe. Obteniendo ultima carrera");
			  carreraActual = carreraRepo.findUltimaCarrera(campeonatoActual.getId());
			  if (carreraActual == null) {
				  log.info("* M1 * No hay carreras para el campeonato " + campeonatoActual.toString() );
			  }
		  }
	    return carreraActual;
	  }
}
