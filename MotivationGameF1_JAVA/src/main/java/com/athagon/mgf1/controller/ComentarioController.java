package com.athagon.mgf1.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.athagon.mgf1.model.Campeonato;
import com.athagon.mgf1.model.Comentario;
import com.athagon.mgf1.repository.ComentarioRepository;

@RestController
@RequestMapping("/comentarios")
public class ComentarioController {

  @Autowired
  private ComentarioRepository repo;
  
  static Date fechaCualquiera = new Date();
  private final Logger log = LoggerFactory.getLogger(this.getClass());

  @RequestMapping(method = RequestMethod.POST)
  public Comentario addComentario(@RequestBody Comentario comentario) {
	  
	  return repo.saveAndFlush(comentario);
  }	  
  @RequestMapping(value = "/campeonato", method = RequestMethod.GET)
  public List<Comentario> getComentariosCampeonato(@RequestBody Campeonato campeonatoActual){
	  
	  List<Comentario> comentarios = repo.findComentariosByCampeonato(campeonatoActual.getId());
	  return comentarios;
  }
  @RequestMapping(value = "/campeonato/cantidad", method = RequestMethod.GET)
  public Comentario getCantidadComentariosCampeonato(@RequestBody Campeonato campeonatoActual){
	  Integer cantidadComentarios = repo.contarComentariosByCampeonato(campeonatoActual.getId());
	  Comentario comentario = new Comentario();	  
	  comentario.setTexto(cantidadComentarios.toString());
	  comentario.setTimestamp(fechaCualquiera);
	  return comentario;
  }
}
