package com.athagon.mgf1.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.athagon.mgf1.model.Empresa;
import com.athagon.mgf1.repository.EmpresaRepository;

/*
 * COMPROBACIÓN CAPTCHA:
 * When your users submit the form where you integrated reCAPTCHA, you'll get as part of the payload a string with the name "g-recaptcha-response". In order to check whether Google has verified that user, send a POST request with these parameters:
	URL: https://www.google.com/recaptcha/api/siteverify
	secret(required)	6LdBwQYTAAAAAIVtk4ZqQZaemKxCNHKsi_VqtwDi
	response(required)	El valor de "g-recaptcha-response".
	remoteip	The end user's ip address.
 */
@RestController
@RequestMapping("/empresas")
public class EmpresaController {

  @Autowired
  private EmpresaRepository repo;
  
  private final Logger log = LoggerFactory.getLogger(this.getClass());	

  /*
  @RequestMapping(method = RequestMethod.GET)
  public List<Empresa> findEmpresas() {
	  log.info("Obteniendo empresas");
    return repo.findAll();
  } */

  @RequestMapping(value = "/{id}", method = RequestMethod.GET)
  public Empresa findEmpresa(@PathVariable Integer id) {
	  Empresa empresa = repo.findOne(id);
	  if (empresa != null) {
		  log.info("* M1 * Obteniendo empresa " + empresa.toString());
	  } else {
		  log.info("* M1 * La empresa con ID " + id + " no existe");
	  }
    return empresa;
  }
  
  @RequestMapping(method = RequestMethod.POST)
  public Empresa addEmpresa(@RequestBody Empresa empresa) {
	  if (empresa.getNombre() != null
			  && empresa.getNombre().length() > 1) {
		  log.info("* M1 * Agragando empresa [" + empresa.getNombre() + "]");
		  empresa.setId(null);
		  return repo.saveAndFlush(empresa);
	  } else {
		  log.info("* M1 * No se puede dar de alta la empresa");
		  return null;
	  }
  }
  
  @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
  public Empresa updateEmpresa(@RequestBody Empresa updatedEmpresa, @PathVariable Integer id) {
	  updatedEmpresa.setId(id);
    return repo.saveAndFlush(updatedEmpresa);
  }
  /*
  @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
  public void deleteEmpresa(@PathVariable Integer id) {
    repo.delete(id);
  } */
}
