package com.athagon.mgf1.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.athagon.mgf1.model.Carrera;
import com.athagon.mgf1.model.LogroCarrera;
import com.athagon.mgf1.model.Participacion;
import com.athagon.mgf1.repository.LogroCarreraRepository;

@RestController
@RequestMapping("/logros")
public class LogroCarreraController {

	  private final Logger log = LoggerFactory.getLogger(this.getClass());
	  
	  @Autowired
	  private LogroCarreraRepository repo;	  

	  @RequestMapping( method = RequestMethod.GET)
	  public LogroCarrera getLogro(@RequestBody LogroCarrera logro) {
		  log.info("* M1 * Buscando logro " + logro.toString());
		  return repo.findOne(logro.getLogroCarreraPK());
	  }

	  @RequestMapping(value = "/campeonato", method = RequestMethod.GET)
	  public  List<LogroCarrera> getLogrosCampeonatoHastaCarreraActual(@RequestBody Participacion participacion) {

		  log.info("* M1 * Obteniendo logros del participante " + participacion.toString());
		  List<LogroCarrera> logros = repo.findLogrosCarerrasByCampeonato(participacion.getParticipacionPK().getCampeonato(), participacion.getParticipacionPK().getParticipante());
		  return logros;
	  }
	  @RequestMapping( method = RequestMethod.POST)
	  public LogroCarrera crearSumarLogro(@RequestBody LogroCarrera logro) {
		  
		  log.info("* M1 * Agragando logro " + logro.toString());		  
		  LogroCarrera logroExistente = repo.findOne(logro.getLogroCarreraPK());
		  
		  if (logroExistente != null) {
			  
			  logro.setLogro(logroExistente.getLogro() + logro.getLogro());
			  logro.setNumeroAleatorio(logroExistente.getNumeroAleatorio());
			  
			  if (logro.getLogro() > logroExistente.getLogro()) {
				  logro.setPuntosExtra(logroExistente.getPuntosExtra() + puntosExtraLogro(logroExistente));
			  } else if (logro.getLogro() < logroExistente.getLogro()) {
				  logro.setPuntosExtra(0);
			  } else {
				  logro.setPuntosExtra(logroExistente.getPuntosExtra());
			  }
		  } else {
			  log.error("* M1 * NO SE HA ENCONTRADO EL LOGRO "+ logro.toString());
		  }
		  Date fechaActual = (new GregorianCalendar()).getTime();
		  if (logro.getFechaPrimerCambio() == null) {
			  logro.setFechaPrimerCambio(fechaActual);		  
		  }
		  logro.setFechaUltimoCambio(fechaActual);
		  logro.setTimestamp(fechaActual);
		  return repo.saveAndFlush(logro);
	  }
	  @RequestMapping( method = RequestMethod.PUT)
	  public List<LogroCarrera> crearRectificarLogros(@RequestBody List<LogroCarrera> logros) {

		  log.info("* M1 * Rectificando logros " + logros.toString());

		  List<LogroCarrera> logrosRectificados = new ArrayList<LogroCarrera>();

		  for (int i = 0; i < logros.size(); i++) {
			  
			  LogroCarrera logro = logros.get(i);
			  LogroCarrera logroExistente = repo.findOne(logro.getLogroCarreraPK());

			  if (logroExistente != null) {
				  
				  logro.setNumeroAleatorio(logroExistente.getNumeroAleatorio());
				  
				  if (logro.getLogro() > logroExistente.getLogro()) {
					  logro.setPuntosExtra(logroExistente.getPuntosExtra() + puntosExtraLogro(logroExistente));
				  } else if (logro.getLogro() < logroExistente.getLogro()) {
					  logro.setPuntosExtra(0);
				  } else {
					  // No cambiamos nada del logro, ponemos los ptos extra como nuevos pero son los mismos
					  logro.setPuntosExtra(logroExistente.getPuntosExtra());
				  }
			  } else {
				  log.error("* M1 * NO SE HA ENCONTRADO EL LOGRO "+ logro.toString());
			  }
			  Date fechaActual = (new GregorianCalendar()).getTime();
			  if (logro.getFechaPrimerCambio() == null) {
				  logro.setFechaPrimerCambio(fechaActual);		  
			  }
			  logro.setFechaUltimoCambio(fechaActual);
			  logro.setTimestamp(fechaActual);
			  
			  logrosRectificados.add(repo.saveAndFlush(logro));
		  } 
		  return logrosRectificados;
	  }
	  /*@RequestMapping( method = RequestMethod.PUT)
	  public LogroCarrera crearRectificarLogro(@RequestBody LogroCarrera logro) {
		  
		  log.info("* M1 * Modificando logro " + logro.toString());
		  LogroCarrera logroExistente = repo.findOne(logro.getLogroCarreraPK());

		  if (logroExistente != null) {
			  if (logro.getLogro() > logroExistente.getLogro()) {
				  logro.setPuntosExtra(logroExistente.getPuntosExtra() + puntosExtraLogro(logroExistente));
			  } else if (logro.getLogro() < logroExistente.getLogro()) {
				  logro.setPuntosExtra(0);
			  } else {
				  logro.setPuntosExtra(logroExistente.getPuntosExtra());
			  }
		  } else {
			  log.error("* M1 * NO SE HA ENCONTRADO EL LOGRO "+ logro.toString());
		  }
		  logro.setTimestamp((new GregorianCalendar()).getTime());
		  return repo.saveAndFlush(logro);
	  }*/
	  /// METODOS PRIVADOS
	  
	  private int puntosExtraLogro(LogroCarrera logro) {

		  int puntosExtra = 0; // Por defecto no hay puntos extra
		  Calendar fechaActual = new GregorianCalendar();
		  Carrera carrera = logro.getCarrera1();

		  // Si esta dentro del margen de fechas de la carrera, continuamos
		  if (fechaActual.getTime().after(carrera.getFechaInicio()) && fechaActual.getTime().before(carrera.getFechaFin())) {

			  // Si aun no hemos insertado logros, o si ha pasado mas de un dia de
			  // la ultima actualziacion, asignamos puntos extra, si no, pues no
			  Calendar fechaLogro = new GregorianCalendar();
			  if (logro.getFechaUltimoCambio() != null) {
				  fechaLogro.setTime(logro.getFechaUltimoCambio());
			  }			  
			  if (		logro.getFechaUltimoCambio() == null 
					  ||
					  	(fechaActual.get(Calendar.DAY_OF_YEAR) > fechaLogro.get(Calendar.DAY_OF_YEAR)
					  	 || fechaActual.get(Calendar.YEAR) > fechaLogro.get(Calendar.YEAR))) {
				  
				  puntosExtra = 1;
				  
				  /* Diferente cantidad de ptos extra seg�n el d�a
				  switch (fechaActual.get(Calendar.DAY_OF_WEEK)) {
				  case Calendar.MONDAY: 	puntosExtra = 7; break;
				  case Calendar.TUESDAY: 	puntosExtra = 6; break;
				  case Calendar.WEDNESDAY:	puntosExtra = 5; break;
				  case Calendar.THURSDAY:	puntosExtra = 4; break;
				  case Calendar.FRIDAY:		puntosExtra = 3; break;
				  case Calendar.SATURDAY:	puntosExtra = 2; break;
				  case Calendar.SUNDAY:		puntosExtra = 1; break;
				  }; */
			  }
		  } 
/*		  // Si ha pasado mas de una semana
		  if ((fechaActual.getTime().getTime() - fechaLogro.getTime().getTime()) / (1000 * 60 * 60 * 24) > 7) {
			  puntosExtra = 0;
		  } else {
			  if (fechaActual.get(Calendar.DAY_OF_YEAR) >= fechaLogro.get(Calendar.DAY_OF_YEAR)
					  || fechaActual.get(Calendar.YEAR) > fechaLogro.get(Calendar.YEAR)) {
				  
				  switch (fechaActual.get(Calendar.DAY_OF_WEEK)) {
				  case Calendar.MONDAY: 	puntosExtra = 7; break;
				  case Calendar.TUESDAY: 	puntosExtra = 6; break;
				  case Calendar.WEDNESDAY:	puntosExtra = 5; break;
				  case Calendar.THURSDAY:	puntosExtra = 4; break;
				  case Calendar.FRIDAY:		puntosExtra = 3; break;
				  case Calendar.SATURDAY:	puntosExtra = 2; break;
				  case Calendar.SUNDAY:		puntosExtra = 1; break;
				  };
			  }
		  } */
		  return puntosExtra;
	  }
}
