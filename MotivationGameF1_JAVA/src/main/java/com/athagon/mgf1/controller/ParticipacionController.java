package com.athagon.mgf1.controller;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.athagon.mgf1.enums.TipoPersona;
import com.athagon.mgf1.model.Campeonato;
import com.athagon.mgf1.model.Participacion;
import com.athagon.mgf1.repository.CampeonatoRepository;
import com.athagon.mgf1.repository.ParticipacionRepository;

@RestController
@RequestMapping("/objetivos")
public class ParticipacionController {

	@Autowired
	private ParticipacionRepository repo;	 
	@Autowired
	private CampeonatoRepository repoCamp;
	  
	private final Logger log = LoggerFactory.getLogger(this.getClass());		
/*
	  @RequestMapping(method = RequestMethod.GET)
	  public List<Participacion> findObjetivos() {
		 log.info("Obteniendo objetivos");
	     return repo.findAll();		
	  }*/
	  @RequestMapping(value = "/porcamp/{idCampeonato}", method = RequestMethod.GET)
	  public List<Participacion> findObjetivosPorCampeonato(@PathVariable Integer idCampeonato) {
		 log.info("* M1 * Obteniendo objetivos del campeonato " + idCampeonato);
	     return repo.findByIdCampeonato(idCampeonato);
	  }
	  
	  @RequestMapping(value = "/porparticipante/{idParticipante}", method = RequestMethod.GET)
	  public HashMap<Integer, Participacion> findObjetivosPorParticipante(@RequestBody Participacion objetivo) {
		 log.info("* M1 * Obteniendo objetivos del Participante " + objetivo.getParticipacionPK().getParticipante());
		  HashMap<Integer, Participacion> objetivosAgregados = new HashMap<Integer, Participacion>();
		  List<Participacion> objetivos = repo.findByIdParticipante(objetivo.getParticipacionPK().getParticipante()); 
		  for (int i = 0; i < objetivos.size(); i++) {
			  objetivosAgregados.put(i, objetivos.get(i));
		  }
	     return objetivosAgregados;		
	  }
	  @RequestMapping(value = "/participantes", method = RequestMethod.POST)
	  public HashMap<Integer, Participacion> addObjetivosParticipantes(@RequestBody HashMap<Integer, Participacion> objetivos) {
		  log.info("* M1 * Agragando participaciones con objetivos " + objetivos.toString());

		  int idCampeonato = objetivos.get(0).getParticipacionPK().getCampeonato();
		  Campeonato campeonato = repoCamp.findOne(idCampeonato);
		  
		  float objetivoTotal = 0;
		  
		  HashMap<Integer, Participacion> objetivosAgregados = new HashMap<Integer, Participacion>();
		  /* 
		   * Comprobar, en caso de duplicar, que el rol es el de mayor rango.
		   */
		  for (int i = 0; i < objetivos.size(); i++) {
			  Participacion participacion = objetivos.get(i);
			  TipoPersona tipoPersona = participacion.getTipo();
			  Participacion participacionExistente = repo.findOne(participacion.getParticipacionPK());
			  if (participacionExistente != null) {
				  //TODO: AQUI CAMBIAMOS EL TIPO
				  if ( participacionExistente.getTipo().compareTo(tipoPersona) > 0 ) {
					  tipoPersona = participacionExistente.getTipo();
				  }				  
			  }
			  participacion.setTipo(tipoPersona);
			  if (tipoPersona != TipoPersona.PIL) {
				  participacion.setObjetivo(0);
			  }
			  objetivosAgregados.put(i, repo.saveAndFlush(participacion));
		  }
		  // Ahora calculamos el total

	     List<Participacion> participacionesGuardadas = repo.findByIdCampeonato(idCampeonato);

		 for (int i = 0; i < participacionesGuardadas.size(); i++) {
			  objetivoTotal += participacionesGuardadas.get(i).getObjetivo();
		 }
		 campeonato.setCantidadObjetivo(objetivoTotal);
		 repoCamp.saveAndFlush(campeonato);
		  return objetivosAgregados;
	  }
	  @RequestMapping(value = "/porparticipante", method = RequestMethod.GET)
	  public Participacion findObjetivoPorParticipante(@RequestBody Participacion objetivo) {
		 log.info("* M1 * Obteniendo objetivo del Participante " + objetivo.getParticipacionPK().getParticipante() + " del campeonato con ID " 
				 	+ objetivo.getParticipacionPK().getCampeonato());
		 Participacion objetivoBuscado = repo.findOne(objetivo.getParticipacionPK());
		 if (objetivoBuscado != null)
			 log.info("* M1 * El objetivo que tiene es "+ objetivoBuscado.getObjetivo());
		 else
			 log.info("* M1 * No se ha encontrado el objetivo " + objetivo.getParticipacionPK());
	     return objetivoBuscado;
	  }
	 /* @RequestMapping(value = "/pilotos/{id}", method = RequestMethod.GET)
	  public List<Persona> findPilotosDeCampeonato(@PathVariable Integer id) {
		  List<Persona> pilotos = repo.findByIdCampeonato(id);
		  if (pilotos != null) {
			  System.out.println("* M1 * findPersonasDeCampeonato " + pilotos.toString());
		  } else {
			  System.out.println("* M1 * ERROR: findPersonasDeCampeonato. El campeonato con ID " + id + " no existe");
		  }
		  return pilotos;
	  }
	  //TODO: Esto está sin probar, y hay que unificar en un sólo método add y actualizar
	  @RequestMapping(method = RequestMethod.POST)
	  public Participacion addParticipacion(@RequestBody Participacion objParticip) {
		  System.out.println("* M1 * Añadiendo " + objParticip.toString());
	    return repo.saveAndFlush(objParticip);
	  }	 
	  //TODO: Esto está sin probar
	  @RequestMapping(value = "/{idCamp,idParticip}", method = RequestMethod.PUT)
	  public Participacion updateParticipacion(@RequestBody Participacion updatedParticipacion, @PathVariable Integer idCamp, @PathVariable Integer idParticip) {
		  ParticipacionPK pkObjP = new ParticipacionPK(idCamp, idParticip);
		  updatedParticipacion.setParticipacionPK(pkObjP);
	    return repo.saveAndFlush(updatedParticipacion);
	  } 
	  //TODO: Esto está sin probar, y hay que hacer como en actualizar
	  @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	  public void deleteCampaign(@PathVariable Integer id) {
	    repo.delete(id);
	  }
	  */
}
