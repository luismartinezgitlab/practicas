package com.athagon.mgf1.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.athagon.mgf1.Configuration;
import com.athagon.mgf1.model.Campeonato;
import com.athagon.mgf1.model.Persona;
import com.athagon.mgf1.repository.PersonaRepository;

@RestController
@RequestMapping("/personas")
public class PersonaController {

  @Autowired
  private PersonaRepository repo;
  private final Logger log = LoggerFactory.getLogger(this.getClass());
  
  private static String[] paletaColores = new String[] {
		 "203,66,66",
		 "110,193,104",
		 "77,77,77",
		 "52,165,201",
		 "255,255,255",
		 "221,162,55",
		 "255,135,152",
		 "189,239,255",
		 "139,167,125",
		 "0,0,0",
		 "170,170,170",
		 "15,72,90",
		 "235,192,98",
		 "255,0,0",
		 "0,255,0",
		 "0,0,255"
  };
  private static Random random = new Random();
  private static String dameColorAleatorio() {
	  return paletaColores[random.nextInt(paletaColores.length)] + "-" + paletaColores[random.nextInt(paletaColores.length)];
  }
  
  /*@RequestMapping(method = RequestMethod.GET)
  public List<Persona> findPersonas() {
	  log.info("Obteniendo Personas");
    return repo.findAll();
  } */

  @RequestMapping(value = "{id}", method = RequestMethod.GET)
  public Persona findPersona(@PathVariable Integer id) {
	  Persona persona = repo.findOne(id);
	  if (persona != null) {
		  persona.setCodigo("");
		  log.info("* M1 * Obteniendo findPersona " + persona.toString());
	  } else {
		  log.info("* M1 * La persona con ID " + id + " no existe");
	  }	  
	  return persona;
  }
  
  // LOGIN
  
  @RequestMapping(value = "/login", method = RequestMethod.GET)
  public Persona findPersonaByEmail(@RequestBody Persona personaLogin) {
	  String email = personaLogin.getEmail();
	  List<Persona> personas = repo.findByEmail(email);
	  if (personas.size() > 0) {
		  //AUTENTIFICACION
		  if (Configuration.getInstance().getCodigosSeguridadActivos()) {
			  if (personaLogin.getCodigo().toUpperCase().equals(personas.get(0).getCodigo().toUpperCase())) {
				  log.info("* M1 * LOGIN OK de " + personaLogin.toString());
				  personaLogin = personas.get(0);
				  personaLogin.setCodigo("LOGIN OK");
			  } else {
				  log.info("* M1 * LOGIN FALLADO de " + personaLogin.toString());
				  personaLogin.setCodigo("LOGIN FALLADO");
			  }
		  } else {
			  personaLogin = personas.get(0);
			  personaLogin.setCodigo("LOGIN OK");			  
		  }
	  }
	  else {
		  log.info("* M1 * La persona con email <" + email + "> no existe");
		  personaLogin.setCodigo("NO EXISTE");
	  }
	  return personaLogin;
  }
  @RequestMapping(method = RequestMethod.POST)
  public Persona addPersona(@RequestBody Persona persona) {
	  List<Persona> perBuscadas = repo.findByEmail(persona.getEmail());
	  if (perBuscadas.size() > 0) {
		  log.info("* M1 * Agragando persona. La persona ya existe: " + perBuscadas.get(0).toString());
		  perBuscadas.get(0).setEmpresa(persona.getEmpresa());
		  return repo.saveAndFlush(perBuscadas.get(0));
	  } else {
		  log.info("* M1 * Agragando nueva persona " + persona.toString());
		  persona.setId(null);
		  persona.setColorCoche(dameColorAleatorio());
		  return repo.saveAndFlush(persona);
	  }
  }
  @RequestMapping(method = RequestMethod.PUT)
  public Persona updatePersona(@RequestBody Persona updatedPersona) {
	  log.info("* M1 * Actualizar persona " + updatedPersona.toString());
	  return repo.saveAndFlush(updatedPersona);
  }
  @RequestMapping(value = "/participantesporcamp", method = RequestMethod.GET)
  public HashMap<Integer, Persona> findParticipantesDeCampeonato(@RequestBody Campeonato campeonato) {
	  List<Persona> pilotos = repo.findByIdCampeonato(campeonato.getId());
	  HashMap<Integer, Persona> dictPilotos = new HashMap<Integer, Persona>();
	  if (pilotos != null) {
		  log.info("* M1 * Recibir participantes de capeonato: " + pilotos.toString());
		  for (int i = 0; i < pilotos.size(); i++) {	
			  pilotos.get(i).setCodigo("");
			  dictPilotos.put(pilotos.get(i).getId(), pilotos.get(i));
		  }
	  } else {
		  log.info("* M1 * ERROR: findPersonasDeCampeonato. El campeonato con ID " + campeonato.getId() + " no existe");
	  }
	  return dictPilotos;
  }
  @RequestMapping(value = "/participantes", method = RequestMethod.POST)
  public HashMap<Integer, Persona> addParticipantes(@RequestBody HashMap<Integer, Persona> pilotos) {
	  log.info("* M1 * Agragando participantes " + pilotos.toString());
	  HashMap<Integer, Persona> pilotosAgregados = new HashMap<Integer, Persona>();
	  int i = 0;
	  for (i = 0; i < pilotos.size(); i++) {
		  
		  Persona piloto = pilotos.get(i);
		  List<Persona> perBuscadasXMail = repo.findByEmail(piloto.getEmail());
		  
		  if (perBuscadasXMail.size() > 0) {
			  perBuscadasXMail.get(0).setObjetivo(piloto.getObjetivo());
			  perBuscadasXMail.get(0).setCodigo("");
			  pilotosAgregados.put(i, perBuscadasXMail.get(0));
		  } else {
			  piloto.setId(null);
			  piloto.setColorCoche(dameColorAleatorio());
			  int objetivo = piloto.getObjetivo();
			  piloto = repo.saveAndFlush(pilotos.get(i));
			  piloto.setObjetivo(objetivo);
			  piloto.setCodigo("");
			  pilotosAgregados.put(i, piloto);
		  }
	  }
	  return pilotosAgregados;
  }  
  /*
  @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
  public void deletePersona(@PathVariable Integer id) {
    repo.delete(id);
  } */
}
