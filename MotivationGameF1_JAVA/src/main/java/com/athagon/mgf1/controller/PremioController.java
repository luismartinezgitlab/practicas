package com.athagon.mgf1.controller;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.athagon.mgf1.model.Campeonato;
import com.athagon.mgf1.model.Premio;
import com.athagon.mgf1.repository.PremioRepository;

@RestController
@RequestMapping("/premios")
public class PremioController {

	  @Autowired
	  private PremioRepository repo;	  

	  private final Logger log = LoggerFactory.getLogger(this.getClass());	
	  /*
	  @RequestMapping(method = RequestMethod.GET)
	  public List<Premio> findPremios() {
		 log.info("* M1 * Obteniendo premios");
	     return repo.findAll();		
	  } */
	  /* @RequestMapping(value = "/porcamp/{idCampeonato}", method = RequestMethod.GET)
	  public List<Premio> findPremiosPorCampeonato(@PathVariable Integer idCampeonato) {
		 log.info("* M1 * Obteniendo Premio del campeonato " + idCampeonato);
	     return repo.findByIdCampeonato(idCampeonato);		
	  }*/
	  @RequestMapping(value = "/porcamp", method = RequestMethod.GET)
	  public HashMap<Integer, Premio> findPremiosPorCampeonato(@RequestBody Campeonato campeonato) {
		 log.info("* M1 * Obteniendo Premio del campeonato " + campeonato.toString());
		  HashMap<Integer, Premio> premiosAgregados = new HashMap<Integer, Premio>();
		  List<Premio> premiosDelCamp = repo.findByIdCampeonato(campeonato.getId());
		  for (int i = 0; i < premiosDelCamp.size(); i++) {
			  premiosAgregados.put(i,premiosDelCamp.get(i));
		  }
		  return premiosAgregados;
	  }
	  @RequestMapping( method = RequestMethod.POST)
	  public HashMap<Integer, Premio> addPremios(@RequestBody HashMap<Integer, Premio> premios) {
		  log.info("* M1 * Agragando premios " + premios.toString());
		  HashMap<Integer, Premio> premiosAgregados = new HashMap<Integer, Premio>();
		  for (int i = 0; i < premios.size(); i++) {
			  premiosAgregados.put(i, repo.saveAndFlush(premios.get(i)));
		  }
		  return premiosAgregados;
	  }
	 /* @RequestMapping(value = "/pilotos/{id}", method = RequestMethod.GET)
	  public List<Persona> findPilotosDeCampeonato(@PathVariable Integer id) {
		  List<Persona> pilotos = repo.findByIdCampeonato(id);
		  if (pilotos != null) {
			  log.info("* M1 * findPersonasDeCampeonato " + pilotos.toString());
		  } else {
			  log.info("* M1 * ERROR: findPersonasDeCampeonato. El campeonato con ID " + id + " no existe");
		  }
		  return pilotos;
	  }
	  //TODO: Esto está sin probar, y hay que unificar en un sólo método add y actualizar
	  @RequestMapping(method = RequestMethod.POST)
	  public Participacion addParticipacion(@RequestBody Participacion objParticip) {
		  log.info("Añadiendo " + objParticip.toString());
	    return repo.saveAndFlush(objParticip);
	  }	 
	  //TODO: Esto está sin probar
	  @RequestMapping(value = "/{idCamp,idParticip}", method = RequestMethod.PUT)
	  public Participacion updateParticipacion(@RequestBody Participacion updatedParticipacion, @PathVariable Integer idCamp, @PathVariable Integer idParticip) {
		  ParticipacionPK pkObjP = new ParticipacionPK(idCamp, idParticip);
		  updatedParticipacion.setParticipacionPK(pkObjP);
	    return repo.saveAndFlush(updatedParticipacion);
	  } 
	  //TODO: Esto está sin probar, y hay que hacer como en actualizar
	  @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	  public void deleteCampaign(@PathVariable Integer id) {
	    repo.delete(id);
	  }
	  */
}
