package com.athagon.mgf1.enums;

public enum TipoEmail {
	ACTIVA, // Activacion
	CODIGO, // Codigos
	INICIO, 	// Inicio
	SOL_LG, 	// Solicitud logro
	RES_CR, 	// Resultado carrera
	SOL_PG, 	//  Solicitud pago
	RES_CP, //  Resultados campeonato provisionales
	RES_CD 	// Resultados campeonato definitivos	
}
