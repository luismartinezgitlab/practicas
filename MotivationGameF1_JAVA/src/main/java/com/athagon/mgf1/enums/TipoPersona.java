package com.athagon.mgf1.enums;

public enum TipoPersona {
	NUL, // Ninguno
    ESP, // ESPECTADOR
    PIL, // PILOTO
    SPN, // SPONSOR
    CON, // Consultor
	ADM, // Administrador
}
