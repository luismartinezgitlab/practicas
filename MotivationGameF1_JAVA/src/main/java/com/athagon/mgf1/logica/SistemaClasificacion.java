package com.athagon.mgf1.logica;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Random;

import com.athagon.mgf1.Configuration;
import com.athagon.mgf1.model.ClasificacionCarrera;
import com.athagon.mgf1.model.PuntosPorPosicion;
import com.athagon.mgf1.repository.ParticipacionRepository;
import com.athagon.mgf1.repository.PuntosPorPosicionRepository;

public class SistemaClasificacion {
	
	static public Random random;
	static protected int[] puntosPorPos;
	
	static protected int puntosPorPosicion(int posicion) {
		if (posicion < puntosPorPos.length) {
			return puntosPorPos[posicion];
		} else {
			return 0;
		}
	}
	protected static ParticipacionRepository participacionRepo;	
	
	protected Comparator<ClasificacionCarrera> comparadorPorPuntosCoberturaYExtra;
	protected Comparator<ClasificacionCarrera> comparadorPorPuntosCarrera;
	protected Comparator<ClasificacionCarrera> comparadorPorPuntosCampeonato;
	
	public SistemaClasificacion(PuntosPorPosicionRepository puntosPPosRepo, ParticipacionRepository participacionRepo) {
		super();
		SistemaClasificacion.participacionRepo = participacionRepo;
		
		if (puntosPorPos == null) {
			random = new Random();
			
			List<PuntosPorPosicion> listaPuntosPPos = puntosPPosRepo.findAll();
			  
			puntosPorPos = new int[listaPuntosPPos.size()];
			  
			for (int j = 0; j < listaPuntosPPos.size(); j++) {
				puntosPorPos[listaPuntosPPos.get(j).getPosicion() - 1] = listaPuntosPPos.get(j).getPuntos();
			}
		}
		comparadorPorPuntosCoberturaYExtra = new Comparator<ClasificacionCarrera>() {
			//TODO: Comprobar, en caso de empate, valorar quien ha metido el dato primero
			
			@Override
			public int compare(ClasificacionCarrera arg0, ClasificacionCarrera arg1) {
				
				if (arg0.getPuntosPorCobertura() + arg0.getPuntosExtra() > arg1.getPuntosPorCobertura() + arg1.getPuntosExtra() )	// Comprobamos por Cobertura
					return -1;
				else if (arg0.getPuntosPorCobertura() + arg0.getPuntosExtra() < arg1.getPuntosPorCobertura() + arg1.getPuntosExtra() )
					return 1;
				else {
					if (arg0.getLogro() > arg1.getLogro())			// Si no, comprobamos por logro bruto
						return -1;
					else if (arg0.getLogro() < arg1.getLogro())
						return 1;
					else {
						if (arg0.getFechaPrimerCambio() == null && arg1.getFechaPrimerCambio() != null) {
							return -1;
						} else if (arg0.getFechaPrimerCambio() != null && arg1.getFechaPrimerCambio() == null) {
							return 1;
						} else if (arg0.getFechaPrimerCambio() == null && arg1.getFechaPrimerCambio() == null) {
							if ( arg0.getNumeroAleatorio() > arg1.getNumeroAleatorio() ) {
								return -1;
							} else {
								return 1;
							}
						} else {
							if (arg0.getFechaPrimerCambio().before(arg1.getFechaPrimerCambio()))  // Si no, comprobamos por fecha introduccion
								return -1;
							else if (arg0.getFechaPrimerCambio().after(arg1.getFechaPrimerCambio())) 
								return 1;
							else {
								if ( arg0.getNumeroAleatorio() > arg1.getNumeroAleatorio() ) {
									return -1;
								} else {
									return 1;
								}
							}							
						}
					}
				}
			}
		};
		comparadorPorPuntosCarrera = new Comparator<ClasificacionCarrera>() {
			//TODO: Comprobar, en caso de empate, valorar quien a metido el dato primero
			@Override
			public int compare(ClasificacionCarrera arg0, ClasificacionCarrera arg1) {
				
				if (arg0.getPuntosCarrera() > arg1.getPuntosCarrera())				// Comprobamos por puntos
					return -1;
				else if (arg0.getPuntosCarrera() < arg1.getPuntosCarrera())
					return 1;
				else 
					return comparadorPorPuntosCoberturaYExtra.compare(arg0, arg1);
			}
		};
	}
	protected void calcularPuntosCobertura(List<ClasificacionCarrera> listaClasifCarr) {

		for (int i = 0; i < listaClasifCarr.size(); i++ ) {

			float coberturaLimitada = listaClasifCarr.get(i).getCobertura();

			if (coberturaLimitada > Configuration.getInstance().getCoberturaMaxima())
				coberturaLimitada = Configuration.getInstance().getCoberturaMaxima();
			
			if (coberturaLimitada >= Configuration.getInstance().getCoberturaMinima()) {

				int puntosPorCobertura = (int) (coberturaLimitada * Configuration.getInstance().getPuntosPorUnidadCobertura());
				listaClasifCarr.get(i).setPuntosPorCobertura(puntosPorCobertura);
			} else {
				listaClasifCarr.get(i).setPuntosPorCobertura(0);
			}
		}
	}
	protected void calcularPuntosCarrera(List<ClasificacionCarrera> listaClasifCarr) {

		Date ahora = new Date();
		Date fechaFin;
		
		for (int posicion = 0; posicion < listaClasifCarr.size(); posicion++ ) {

			ClasificacionCarrera clasifCarr = listaClasifCarr.get(posicion); 
			
			clasifCarr.setPosicionPorCobertura(posicion + 1);
			fechaFin = clasifCarr.getCarreraFechaFin();
			
			// Aqu� no sumamos los puntos por posici�n hasta que no haya pasado la semana
			if ( ahora.after(fechaFin) ) {

				if (clasifCarr.getPuntosPorCobertura()+clasifCarr.getPuntosExtra() > 0)
					clasifCarr.setPuntosPorPosicion(puntosPorPosicion(posicion));
				else
					clasifCarr.setPuntosPorPosicion(0);	
			} else {
				clasifCarr.setPuntosPorPosicion(0);
			}
			
			clasifCarr.setPuntosCarrera(
					  clasifCarr.getPuntosPorCobertura()
					+ clasifCarr.getPuntosPorPosicion()
					+ clasifCarr.getPuntosExtra());
		}
	}
	protected void calcularPosicionesYPuntosOrdenando(List<ClasificacionCarrera> listaClasifCarr) {

		calcularPuntosCobertura(listaClasifCarr);
		
		Collections.sort(listaClasifCarr, comparadorPorPuntosCoberturaYExtra);
		
		calcularPuntosCarrera(listaClasifCarr);
		
		// Esta linea puede que sobre, ya deberian estar ordenados
		
		Collections.sort(listaClasifCarr, comparadorPorPuntosCarrera);

		for (int puesto = 1; puesto <= listaClasifCarr.size(); puesto++ ) {
			listaClasifCarr.get(puesto - 1).setPuesto(puesto);
		}
	}
}
