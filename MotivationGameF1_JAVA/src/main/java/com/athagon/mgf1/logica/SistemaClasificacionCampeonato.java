package com.athagon.mgf1.logica;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import com.athagon.mgf1.model.ClasificacionCarrera;
import com.athagon.mgf1.repository.ParticipacionRepository;
import com.athagon.mgf1.repository.PuntosPorPosicionRepository;

public class SistemaClasificacionCampeonato extends SistemaClasificacion  {
	
	public SistemaClasificacionCampeonato(PuntosPorPosicionRepository puntosPPosRepo, ParticipacionRepository participacionRepo) {
		super(puntosPPosRepo, participacionRepo);
		
		// AQUI HAY QUE COMPARAR POR PUNTOS TOTALES, LUEGO POR TIEMPO Y LUEGO POR LOGRO ACUMULADO
		
		comparadorPorPuntosCampeonato = new Comparator<ClasificacionCarrera>() {
			@Override
			public int compare(ClasificacionCarrera arg0, ClasificacionCarrera arg1) {
				
				if (arg0.getCarrera() == arg1.getCarrera()) {	// Comprobamos que sea la misma carrera 
					if (arg0.getPuntosCampeonato() > arg1.getPuntosCampeonato())		// Comprobamos por puntos de campeontao
						return -1;
					else if (arg0.getPuntosCampeonato() < arg1.getPuntosCampeonato())
						return 1;
					else {
						if ( arg0.getCoberturaAcumulada() > arg1.getCoberturaAcumulada() )			// Si no, comprobamos por cobertura acumulada
							return -1;
						else if ( arg0.getCoberturaAcumulada() < arg1.getCoberturaAcumulada() )
							return 1;
						else {
							if ( arg0.getLogroAcumulado() > arg1.getLogroAcumulado() )			// Si no, comprobamos por logro bruto
								return -1;
							else if ( arg0.getLogroAcumulado() < arg1.getLogroAcumulado() )
								return 1;
							else {
								if (arg0.getFechaPrimerCambio() == null && arg1.getFechaPrimerCambio() != null) {
									return -1;
								} else if (arg0.getFechaPrimerCambio() != null && arg1.getFechaPrimerCambio() == null) {
									return 1;
								} else if (arg0.getFechaPrimerCambio() == null && arg1.getFechaPrimerCambio() == null) {
									if ( arg0.getNumeroAleatorio() > arg1.getNumeroAleatorio() ) {
										return -1;
									} else {
										return 1;
									}
								} else {
									if (arg0.getFechaPrimerCambio().before(arg1.getFechaPrimerCambio()))  // Si no, comprobamos por fecha introduccion
										return -1;
									else if (arg0.getFechaPrimerCambio().after(arg1.getFechaPrimerCambio())) 
										return 1;
									else {
										if ( arg0.getNumeroAleatorio() > arg1.getNumeroAleatorio() ) {
											return -1;
										} else {
											return 1;
										}
									}							
								}
							}
						}
					}
				} else if (arg0.getCarrera() < arg1.getCarrera())	// Si no, Comprobamos por carrera
					return -1;
				else
					return 1;
			}
		};
	}
	private class PuntosYLogroAcumulado {
		public int puntosCampeonato;
		public int logroAcumulado;
		public float coberturaAcumulada;
		public float desviacionAcumulada;
	}
	public List<ClasificacionCarrera> calcularPosicionesCampeonato(List<ClasificacionCarrera> clasifCarrByCampOriginal) {
//		System.out.println("calcularPosicionesCampeonato()");
		ArrayList<Integer> arrayIdsCarreras = new ArrayList<Integer>();
		HashMap<Integer, List<ClasificacionCarrera>> clasifCampeonatoPorCarrera = new HashMap<Integer, List<ClasificacionCarrera>>();

		// Organizamos en un diccionario de clasificaciones por carrera
		for (int i = 0; i < clasifCarrByCampOriginal.size(); i++ ) {
			
			int idCarrera = clasifCarrByCampOriginal.get(i).getCarrera();
			
			if ( ! clasifCampeonatoPorCarrera.containsKey( idCarrera )) {
				
				clasifCampeonatoPorCarrera.put(idCarrera, new ArrayList<ClasificacionCarrera>());
				arrayIdsCarreras.add(idCarrera);
			}
			clasifCampeonatoPorCarrera.get(idCarrera).add(clasifCarrByCampOriginal.get(i));
		}
		Collections.sort(arrayIdsCarreras, new Comparator<Integer>() {
			@Override
			public int compare(Integer o1, Integer o2) {				
				return o1 < o2 ? -1 : o1 > o2 ? 1 : 0;
			}
		});
		// CALCULAR EL ACUMULADO
		HashMap<Integer, PuntosYLogroAcumulado> puntuacionesAcumulAnt = new HashMap<Integer, PuntosYLogroAcumulado>();
		
		//for (Map.Entry<Integer, List<ClasificacionCarrera>> entryListaCCarr : clasifCampeonatoPorCarrera.entrySet()) {
		for (int nCarr = 0; nCarr < arrayIdsCarreras.size(); nCarr++) {
			List<ClasificacionCarrera> listaClasifCarr = clasifCampeonatoPorCarrera.get(arrayIdsCarreras.get(nCarr));			
			
			calcularPosicionesYPuntosOrdenando(listaClasifCarr);

			for (int i = 0; i < listaClasifCarr.size(); i++ ) {
				if ( !puntuacionesAcumulAnt.containsKey(listaClasifCarr.get(i).getPiloto())) {
					puntuacionesAcumulAnt.put(listaClasifCarr.get(i).getPiloto(), new PuntosYLogroAcumulado());
				}
				PuntosYLogroAcumulado ptsLogro = puntuacionesAcumulAnt.get(listaClasifCarr.get(i).getPiloto());
				ptsLogro.puntosCampeonato += listaClasifCarr.get(i).getPuntosCarrera();
				ptsLogro.logroAcumulado += listaClasifCarr.get(i).getLogro();
				ptsLogro.coberturaAcumulada += listaClasifCarr.get(i).getCobertura();
				ptsLogro.desviacionAcumulada += listaClasifCarr.get(i).getDesviacion();
				listaClasifCarr.get(i).setPuntosCampeonato( ptsLogro.puntosCampeonato);
				listaClasifCarr.get(i).setLogroAcumulado( ptsLogro.logroAcumulado);
				listaClasifCarr.get(i).setCoberturaAcumulada( ptsLogro.coberturaAcumulada);
				listaClasifCarr.get(i).setDesviacionAcumulada( ptsLogro.desviacionAcumulada);
				// System.out.println(listaClasifCarr.get(i).getCarrera() + ") ["+ listaClasifCarr.get(i).getPiloto() +"] Cobertura = " + listaClasifCarr.get(i).getCobertura() + ", Acumulada = " + ptsLogro.coberturaAcumulada + ", Logro = " + listaClasifCarr.get(i).getLogro() + ", Acumulado = " + ptsLogro.logroAcumulado);
			}
		}
		// Ordenamos por los puntos de campeonato
		Collections.sort (clasifCarrByCampOriginal, comparadorPorPuntosCampeonato);
		
		// Marcamos los puestos de campeonato
		int carrera = clasifCarrByCampOriginal.get(0).getCarrera();
		int puesto = 1;
		for (int i = 0; i < clasifCarrByCampOriginal.size(); i++ ) {
			if (clasifCarrByCampOriginal.get(i).getCarrera() != carrera) {
				carrera = clasifCarrByCampOriginal.get(i).getCarrera();
				puesto = 1;
			}
			clasifCarrByCampOriginal.get(i).setPuestoCampeonato(puesto);
			puesto ++;
		}
		return clasifCarrByCampOriginal;
	}
}
