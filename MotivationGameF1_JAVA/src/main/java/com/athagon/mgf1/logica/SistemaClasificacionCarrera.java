package com.athagon.mgf1.logica;

import java.util.List;

import com.athagon.mgf1.model.ClasificacionCarrera;
import com.athagon.mgf1.repository.ParticipacionRepository;
import com.athagon.mgf1.repository.PuntosPorPosicionRepository;

public class SistemaClasificacionCarrera extends SistemaClasificacion {
	
	public SistemaClasificacionCarrera(PuntosPorPosicionRepository puntosPPosRepo, ParticipacionRepository participacionRepo) {
		super(puntosPPosRepo, participacionRepo);
	}
	public List<ClasificacionCarrera> calcularPosicionesCarrera(List<ClasificacionCarrera> clasifCarrByCarrera) {

		calcularPosicionesYPuntosOrdenando(clasifCarrByCarrera);

		return clasifCarrByCarrera;
	}
}
