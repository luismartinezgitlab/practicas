package com.athagon.mgf1.logica;

import java.util.Calendar;
import java.util.Date;

public class SistemaFechas {
	public static int getDayOfMonth(Date aDate) {
	    Calendar cal = Calendar.getInstance();
	    cal.setTime(aDate);
	    return cal.get(Calendar.DAY_OF_MONTH);
	}
}
