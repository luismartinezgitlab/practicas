package com.athagon.mgf1.mail;

import com.athagon.mail.SMTPAcumbamail;
import com.athagon.mail.SMTPMailClient;


public class AcumbamailSMTPtest {
    
    public static final String EMAIL_FROM  = "motivationGameF1@athagon.com";
    
    private static final String[] EMAILS_TO  = {"probando@athagon.com", "probando_2@athagon.com", "gcaballero@athagon.com" };
    private static final String EMAIL_TO_2  = "jmgonzalezdelalamo@gmail.com";
    private static final String[] EMAILS_TO_CC  = {"jmgonzalezdelalamo@gmail.com", "probando_4@athagon.com", "germanux@gmail.com"};

    public void test(int numCorreo) throws Exception{
        System.out.println(">>> Enviando correo desde "+EMAIL_FROM );
        
        SMTPMailClient clienteMail = new SMTPAcumbamail();

        String cuerpoMail = "<b>Testeando servidor de mail Acumbamail</b>  Prueba " + numCorreo;
        cuerpoMail += "<br><br>Ya puedes incluir los resultados de la carrera.<br><br><b>Campa�a:</b>\tVenta de coches ...";
        clienteMail.crearMail("E-Mail desde Java - Motivation Game F1 Mailing System", cuerpoMail, EMAIL_FROM, true);
        
        clienteMail.addDestinatarios(EMAILS_TO);
        clienteMail.addDestinatario(EMAIL_TO_2);
        clienteMail.addDestinatariosCC(EMAILS_TO_CC);
        
        clienteMail.enviarMail();
        
        System.out.println("Correo enviado. <<<");
    }
}
