package com.athagon.mgf1.mail;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.hibernate.Hibernate;

import com.athagon.mail.LectorPlantilla;
import com.athagon.mail.SMTPAcumbamail;
import com.athagon.mail.SMTPMailClient;
import com.athagon.mgf1.Configuration;
import com.athagon.mgf1.enums.EstadoMail;
import com.athagon.mgf1.enums.TipoEmail;
import com.athagon.mgf1.enums.TipoPersona;
import com.athagon.mgf1.model.Campeonato;
import com.athagon.mgf1.model.EnvioEmail;
import com.athagon.mgf1.model.Participacion;
import com.athagon.mgf1.model.Persona;
import com.athagon.mgf1.model.Premio;
import com.athagon.mgf1.repository.EnvioEmailRepository;
import com.athagon.mgf1.repository.PersonaRepository;

public class GestorMailing {

	private SMTPAcumbamail clienteMail;
	private EnvioEmailRepository repoMails;
	private PersonaRepository repoPersonas;
	private int minutosEnvioEmailDesdeCreacion;
	private int horasEnvioEmailSegundoIntento;
	private int cantidadEmailsEnviadosPorVez;
	
	private String[] arrayCampos = {
			"Nombre_participante",
			"Nombre_empresa",
			"Nombre_campeonato",
			"Nombre_sponsor",
			"Fecha_inicio_campeonato",
			"Fecha_fin_campeonato",
			"Numero_carreras",
			"email@sponsor",	
			"Dias_inicio_campeonato",
			"Numero_pilotos",
			"Numero_espectadores",
			"Tipo_objetivo",
			"Cuantia_objetivo",
			"Cuantia_objetivo_conseguida",
			"Cuantia_objetivo_global",
			"Cuantia_objetivo_global_conseguida",
			"Posicion_campeonato",
			"Codigos_acceso"
	};
	private String[] arrayCamposComunes = {
			"Nombre_participante",
			"Nombre_empresa",
			"Nombre_campeonato",
			"Nombre_sponsor",
			"Fecha_inicio_campeonato",
			"Fecha_fin_campeonato",
			"Numero_carreras",
			"email@sponsor",
			"email@soporte",
	};
	private String[] arrayCamposCampeonato = {
			"Dias_inicio_campeonato",
			"Numero_pilotos",
			"Numero_espectadores",
			"Tipo_objetivo",
			"Cuantia_objetivo",
			"Cuantia_objetivo_conseguida",
			"Cuantia_objetivo_global",
			"Cuantia_objetivo_global_conseguida"
	};
	private String[] arrayCamposActivacion = {
			"Premio_inic",
			"Numero_pilotos",
			"Numero_espectadores",
			"Tipo_objetivo",
			"Cuantia_objetivo",
			"Cuantia_objetivo_conseguida",
			"Cuantia_objetivo_global",
			"Cuantia_objetivo_global_conseguida",
			"Codigos_acceso"
	};
	private String CAMPO_PREMIO_CAMP = "Premio_campeonato";
	private String CAMPO_PREMIO_CARR = "Premio_carrera";
	private String CAMPO_GANADOR_CAMP = "Ganador_campeonato";
	private String CAMPO_GANADOR_CARR = "Ganador_carrera";
	
	public GestorMailing(EnvioEmailRepository repoMails, PersonaRepository repoPersonas, int minutosEnvioEmailDesdeCreacion, int horasEnvioEmailSegundoIntento, int cantidadEmailsEnviadosPorVez) {
		  clienteMail = new SMTPAcumbamail();
		  this.repoMails = repoMails;
		  this.repoPersonas = repoPersonas;
		  this.minutosEnvioEmailDesdeCreacion = minutosEnvioEmailDesdeCreacion;
		  this.horasEnvioEmailSegundoIntento = horasEnvioEmailSegundoIntento;
		  this.cantidadEmailsEnviadosPorVez = cantidadEmailsEnviadosPorVez;
	}
	public void enviarEmailsPendientes() {
		
		Calendar maxTimestamp = new GregorianCalendar();
		maxTimestamp.add(Calendar.MINUTE, -minutosEnvioEmailDesdeCreacion);

		List<EnvioEmail> emailsEnEspera = repoMails.findByEstado(EstadoMail.PRE.toString(), maxTimestamp.getTime(), cantidadEmailsEnviadosPorVez);
		//List<EnvioEmail> emailsEnEspera = repoMails.findByEstado(maxTimestamp.getTime(), cantidadEmailsEnviadosPorVez);
		
		enviarListadoMails(emailsEnEspera, EstadoMail.ERR_1);
		
		Calendar dentroDeUnaHora = maxTimestamp;
		dentroDeUnaHora.add(Calendar.HOUR_OF_DAY, -horasEnvioEmailSegundoIntento);

		List<EnvioEmail> emailsFallidos = repoMails.findByEstado(EstadoMail.ERR_1.toString(), dentroDeUnaHora.getTime(), cantidadEmailsEnviadosPorVez);
		//List<EnvioEmail> emailsFallidos = repoMails.findByEstado(/*EstadoMail.ERR_1, */ dentroDeUnaHora.getTime(), cantidadEmailsEnviadosPorVez);

		enviarListadoMails(emailsFallidos, EstadoMail.ERR_2);		
	}
	void enviarListadoMails(List<EnvioEmail> emailsPendienets, EstadoMail nuevoEstadoFallo) {
		Calendar calendarioActual = new GregorianCalendar();

		String dia = Integer.toString(calendarioActual.get(Calendar.DATE));
		String mes = Integer.toString(calendarioActual.get(Calendar.MONTH));
		String annio = Integer.toString(calendarioActual.get(Calendar.YEAR));
		
		if ( emailsPendienets.size() > 0) {

	        int contadorEnvios = 0;
	        
			for (int i = 0; i < emailsPendienets.size(); i++) {
				
				EnvioEmail envioEmail = emailsPendienets.get(i);
	
				if (envioEmail.getFechaHoraEnvio().before((new GregorianCalendar().getTime()))) {
					
					Campeonato campeonato = envioEmail.getCampeonato();
										
					Hibernate.initialize(campeonato);
					
					Set<Participacion> objetivosCampeonato = campeonato.getParticipaciones();

					Persona sponsor = campeonato.getSponsor();
					
					if (sponsor != null) {
						Object[] premios = campeonato.getPremios().toArray();
						
						boolean todosMailsEnviados = true;					
	
						String[] arrayValores = {
								"", // "Nombre_participante",
								sponsor != null ? sponsor.getEmpresa().getNombre() : "SIN EMPRESA ASIGNADA", // "Nombre_empresa",
								campeonato.getNombre(), // "Nombre_campeonato",
								sponsor != null ? sponsor.getNombre() + " " + sponsor.getApellidos() : "SIN SPONSOR ASIGNADO", // "Nombre_sponsor",
								campeonato.getFechaInicio().toString(), // "Fecha_inicio_campeonato",
								campeonato.getFechaFin().toString(), // "Fecha_fin_campeonato",
								campeonato.getNumCarreras() + "", // "Numero_carreras",
								sponsor != null ? sponsor.getEmail() : "SIN SPONSOR ASIGNADO" , // "email@sponsor",	
								getDateDiff(campeonato.getFechaInicio(), calendarioActual.getTime(), TimeUnit.DAYS) + "", //TODO: "Dias_inicio_campeonato",
								55 + "", //TODO: , // "Numero_pilotos",
								77 + "", //TODO: , // "Numero_espectadores",
								campeonato.getTipoObjetivo(), // "Tipo_objetivo",
								550055 + "", // "Cuantia_objetivo",
								440044 + "", // "Cuantia_objetivo_conseguida",
								campeonato.getCantidadObjetivo() + "", // "Cuantia_objetivo_global",
								770077 + "", // "Cuantia_objetivo_global_conseguida",
								55 + "", // "Posicion_campeonato"
								000000 + "", // Codigos_acceso
						};
						ArrayList<String> premiosCamp = new ArrayList<String>();
						ArrayList<String> premiosCarr = new ArrayList<String>();
						
						if (premios.length > 0) {
							for (int p = 0; p < premios.length; p++) {
								switch (((Premio)premios[p]).getTipo()) {
								case CAMP:
									premiosCamp.add(((Premio)premios[p]).getNombre());
									break;
								case CARR:
									premiosCarr.add(((Premio)premios[p]).getNombre());
									break;
								}
							}
						}
						int numPremios = premiosCamp.size();
						for (int p = 0; p < 3 - numPremios; p++) {
							premiosCamp.add("No hay premio");
						}
						numPremios = premiosCarr.size();
						for (int p = 0; p < 3 - numPremios; p++) {
							premiosCarr.add("No hay premio");
						}
						boolean enviarASponsor = true;
						boolean enviarAPilotos = true;
						boolean enviarAEspectadores = true;
						
				        LectorPlantilla lectorPlantilla = null;
				        
						switch (envioEmail.getTipoEmail()) {
						case ACTIVA:
							lectorPlantilla = new LectorPlantilla(
									Configuration.getInstance().getFichMailActivacion(), 
									Configuration.getInstance().getAsuntoMailActivacion(), 
									arrayCampos);
							break;
						case CODIGO:
							lectorPlantilla = new LectorPlantilla(
									Configuration.getInstance().getFichMailCodigos(), 
									Configuration.getInstance().getAsuntoMailCodigos(), 
									arrayCampos);
							enviarAPilotos = false;
							enviarAEspectadores = false;
							break;
						case INICIO:
							lectorPlantilla = new LectorPlantilla(
									Configuration.getInstance().getFichMailPreavisoInicio(), 
									Configuration.getInstance().getAsuntoMailPreavisoInicio(), 
									arrayCampos);
							break;
						case SOL_LG:
							lectorPlantilla = new LectorPlantilla(
									Configuration.getInstance().getFichMailSolicitudLogro(),  
									Configuration.getInstance().getAsuntoMailSolicitudLogro(), 
									arrayCampos);
							enviarASponsor = false;
							enviarAEspectadores = false;
							break;
						case RES_CR:
							lectorPlantilla = new LectorPlantilla(
									Configuration.getInstance().getFichMailResultadosCarrera(),  
									Configuration.getInstance().getAsuntoMailResultadosCarrera(), 
									arrayCampos);
							break;
						case SOL_PG:
							lectorPlantilla = new LectorPlantilla(
									Configuration.getInstance().getFichMailSolicitudPago(),  
									Configuration.getInstance().getAsuntoMailSolicitudPago(), 
									arrayCampos);
							enviarAPilotos = false;
							enviarAEspectadores = false;
							break;
						case RES_CP:
							lectorPlantilla = new LectorPlantilla(
									Configuration.getInstance().getFichMailResultadosProv(),  
									Configuration.getInstance().getAsuntoMailResultadosProv(), 
									arrayCampos);
							break;
						case RES_CD:
							lectorPlantilla = new LectorPlantilla(
									Configuration.getInstance().getFichMailResultadosDef(),  
									Configuration.getInstance().getAsuntoMailResultadosDef(), 
									arrayCampos);
							break;
						default:
							break;
						}
						String remitente = SMTPMailClient.combinarNombreYmail(sponsor.getNombre(), sponsor.getApellidos(), sponsor.getEmail());
						
						for (Participacion participacionObjetivo : objetivosCampeonato) {
							
							if (	(participacionObjetivo.getTipo() == TipoPersona.SPN 
										|| participacionObjetivo.getTipo() == TipoPersona.CON
										|| participacionObjetivo.getTipo() == TipoPersona.ADM) && enviarASponsor
									|| participacionObjetivo.getTipo() == TipoPersona.PIL && enviarAPilotos
									|| participacionObjetivo.getTipo() == TipoPersona.ESP && enviarAEspectadores) {

								Persona participante = participacionObjetivo.getPersona();
		
								arrayValores[0] = participante.getNombre()+" " + participante.getApellidos();
								
								switch (participacionObjetivo.getTipo()) {
								case PIL:
									arrayValores[12] = participacionObjetivo.getObjetivo() + "";
									break;
								case SPN:
									arrayValores[12] = "SPONSOR";
									break;
								case ESP:
									arrayValores[12] = "ESPECTADOR";
									break;
								case CON:
									arrayValores[12] = "CONSULTOR";
									break;
								case ADM:
									arrayValores[12] = "ADMINISTRADOR";
									break;
								default:
									arrayValores[12] = "ERROR EN MAIL";
									break;
								}
								if (envioEmail.getTipoEmail() == TipoEmail.CODIGO) {

									arrayValores[17] = getTablaCodigos(objetivosCampeonato, campeonato.getTipoObjetivo());
								}	
								lectorPlantilla.restaurarDesdePlantilla();
								lectorPlantilla.insertarValores(arrayValores);
								lectorPlantilla.insertarArrayValores(CAMPO_PREMIO_CAMP, premiosCamp.toArray());
								lectorPlantilla.insertarArrayValores(CAMPO_PREMIO_CARR, premiosCarr.toArray());
									
								if (participacionObjetivo.getTipo() != TipoPersona.PIL) {
									//TODO: Quitar lo que estÃ© entre [PIL] y [/PIL]
								}
								String destinatario = SMTPMailClient.combinarNombreYmail(participante.getNombre(), participante.getApellidos(), participante.getEmail());
									
							    clienteMail.crearMail(lectorPlantilla.getAsunto(), lectorPlantilla.getHTML(), remitente, campeonato.getCategoriaAcumbamail());
							        
							    clienteMail.addDestinatario(destinatario);
							        
							    clienteMail.enviarMail();
							        
							    if ( ! clienteMail.enviado()) {
							       	todosMailsEnviados = false;
							    }
								contadorEnvios++;	
							}
						}
				        if ( todosMailsEnviados ) {
				        	envioEmail.setEstadoMail(EstadoMail.ENV);
				        } else {
				        	envioEmail.setEstadoMail(nuevoEstadoFallo);	        	
				        }
				        repoMails.saveAndFlush(envioEmail);
					}
				} else {	
					SMTPAcumbamail.logError(" SE HA COLADO UN MAIL QUE NO DEBERIA ENVIARSE ");
				}
			}
			if (contadorEnvios > 0) {
				calendarioActual = new GregorianCalendar();
				SMTPAcumbamail.log(" FIN DEL ENVIO DE " + contadorEnvios + " EMAILS DEL TIRON: " + calendarioActual.getTime());
			}
		}
	}
	/**
	 * Get a diff between two dates
	 * @param date1 the oldest date
	 * @param date2 the newest date
	 * @param timeUnit the unit in which you want the diff
	 * @return the diff value, in the provided unit
	 */
	public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
	    long diffInMillies = date2.getTime() - date1.getTime();
	    return timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS);
	}
	private String getTablaCodigos(Set<Participacion> objetivosCampeonato, String tipoObjetivo) {
		String tablaCodigos = "<TABLE>";
		tablaCodigos += "<thead><tr><th>APELLIDOS, NOMBRE E E-MAIL</th><th>OBJETIVOS Y CODIGO</th></tr></thead>";
		tablaCodigos += "<tbody>";
						
		for (Participacion objetivo : objetivosCampeonato) {
			Persona participante = objetivo.getPersona();
			tablaCodigos += "<tr><td><b>";
			tablaCodigos += participante.getApellidos() +", " + participante.getNombre() + "</b></td><td>";
			if (objetivo.getTipo().equals(TipoPersona.PIL)) {
				tablaCodigos += objetivo.getObjetivo() + " " + tipoObjetivo;
			} else {
				if (objetivo.getTipo().equals(TipoPersona.SPN)) {
					tablaCodigos += "Sponsor";
				} else if (objetivo.getTipo().equals(TipoPersona.ESP)) {
					tablaCodigos += "Espectador";
				} else if (objetivo.getTipo().equals(TipoPersona.ADM)) {
					tablaCodigos += "Administrador";
				} else if (objetivo.getTipo().equals(TipoPersona.CON)) {
					tablaCodigos += "Consultor";
				}
			}
			tablaCodigos += "</td></tr><tr><td>";
			tablaCodigos += participante.getEmail() + "</td><td>" + participante.getCodigo();
			tablaCodigos += "<br><br></td></tr>";
		}
		tablaCodigos += "</tbody></TABLE>";
		return tablaCodigos;
	}
}
