/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.athagon.mgf1.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.athagon.mgf1.enums.TipoPersona;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 *
 * @author Athagon_German
 */
@Entity
// @XmlRootElement
@NamedQueries({ @NamedQuery(name = "Campeonato.findAll", query = "SELECT c FROM Campeonato c"),
		@NamedQuery(name = "Campeonato.findById", query = "SELECT c FROM Campeonato c WHERE c.id = :id"),
		@NamedQuery(name = "Campeonato.findByNombre", query = "SELECT c FROM Campeonato c WHERE c.nombre = :nombre"),
		@NamedQuery(name = "Campeonato.findByFechaInicio", query = "SELECT c FROM Campeonato c WHERE c.fechaInicio = :fechaInicio"),
		@NamedQuery(name = "Campeonato.findByFechaFin", query = "SELECT c FROM Campeonato c WHERE c.fechaFin = :fechaFin"),
		@NamedQuery(name = "Campeonato.findByNumCarreras", query = "SELECT c FROM Campeonato c WHERE c.numCarreras = :numCarreras"),
		@NamedQuery(name = "Campeonato.findByTipoObjetivo", query = "SELECT c FROM Campeonato c WHERE c.tipoObjetivo = :tipoObjetivo"),
		@NamedQuery(name = "Campeonato.findByCantidadObjetivo", query = "SELECT c FROM Campeonato c WHERE c.cantidadObjetivo = :cantidadObjetivo"),
		@NamedQuery(name = "Campeonato.findByCategoriaAcumbamail", query = "SELECT c FROM Campeonato c WHERE c.categoriaAcumbamail = :categoriaAcumbamail"),
		@NamedQuery(name = "Campeonato.findByTimestamp", query = "SELECT c FROM Campeonato c WHERE c.timestamp = :timestamp") })
public class Campeonato implements Serializable {
	// METER LOGRO ACUMULADO
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Basic(optional = false)
	private Integer id;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 40)
	private String nombre;
	@Basic(optional = false)
	@NotNull
	@Temporal(TemporalType.DATE)
	private Date fechaInicio;
	@Basic(optional = false)
	@NotNull
	@Temporal(TemporalType.DATE)
	private Date fechaFin;
	@Basic(optional = false)
	@NotNull
	private int numCarreras;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 40)
	@Column(name = "tipo_objetivo")
	private String tipoObjetivo;
	@Basic(optional = false)
	@NotNull
	private float cantidadObjetivo;
	@Size(max = 40)
	private String categoriaAcumbamail;
	@Basic(optional = false)
	@Column(name = "timestamp", nullable = false, updatable = false, insertable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	@Temporal(TemporalType.TIMESTAMP)
	private Date timestamp;
	// Versión autogenerada
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "campeonato1", fetch = FetchType.EAGER)
	// Versión ALVARO
	// @OneToMany (fetch = FetchType.EAGER)
	// @JoinColumn(name="campeonato1", insertable=false, updatable=false)
	@JsonManagedReference(value = "campeonato")
	private Set<Participacion> participaciones;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "campeonato1", fetch = FetchType.EAGER)
	@JsonManagedReference(value = "campeonato")
	private Set<Premio> premios;

	// @OneToMany(cascade = CascadeType.ALL, mappedBy = "campeonato")
	// private Set<EnvioEmail> enviosEmails;
	// @OneToMany(cascade = CascadeType.ALL, mappedBy = "campeonato")
	// private Set<Carrera> carreras;

	public Campeonato() {
	}

	public Campeonato(Integer id) {
		this.id = id;
	}

	public Campeonato(Integer id, String nombre, Date fechaInicio, Date fechaFin, int numCarreras, String tipoObjetivo,
			long cantidadObjetivo, Date timestamp) {
		this.id = id;
		this.nombre = nombre;
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
		this.numCarreras = numCarreras;
		this.tipoObjetivo = tipoObjetivo;
		this.cantidadObjetivo = cantidadObjetivo;
		this.timestamp = timestamp;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public int getNumCarreras() {
		return numCarreras;
	}

	public void setNumCarreras(int numCarreras) {
		this.numCarreras = numCarreras;
	}

	public String getTipoObjetivo() {
		return tipoObjetivo;
	}

	public void setTipoObjetivo(String tipoObjetivo) {
		this.tipoObjetivo = tipoObjetivo;
	}

	public float getCantidadObjetivo() {
		return cantidadObjetivo;
	}

	public void setCantidadObjetivo(float cantidadObjetivo) {
		this.cantidadObjetivo = cantidadObjetivo;
	}
	public String getCategoriaAcumbamail() {
		return categoriaAcumbamail;
	}

	public void setCategoriaAcumbamail(String categoriaAcumbamail) {
		this.categoriaAcumbamail = categoriaAcumbamail;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public Set<Participacion> getParticipaciones() {
		return participaciones;
	}

	public void setParticipaciones(Set<Participacion> participaciones) {
		this.participaciones = participaciones;
	}
	@Transient
	//@XmlTransient
	@JsonIgnore
	public Persona getSponsor() {
		int i = 0;
	    for(Participacion p:getParticipaciones()){
	      if (p.getTipo().equals(TipoPersona.SPN)) {
	    	  return p.getPersona();
	      }
	    }
	    System.out.println("ERROR!! No se ha encontrado el sponsor del campeonato " + id);
	    return null;
	}
	// @XmlTransient
	public Set<Premio> getPremios() {
		return premios;
	}

	public void setPremios(Set<Premio> premios) {
		this.premios = premios;
	}
	/*
	 * public Premio addPremio(Premio premio) { getPremios().add(premio);
	 * premio.setCampeonato1(this);
	 * 
	 * return premio; }
	 * 
	 * public Premio removePremio(Premio premio) { getPremios().remove(premio);
	 * premio.setCampeonato1(null);
	 * 
	 * return premio; }
	 */
	//
	// public Set<EnvioEmail> getEnvioEmails() {
	// return envioEmails;
	// }
	//
	// public void setEnvioEmails(Set<EnvioEmail> envioEmails) {
	// this.envioEmails = envioEmails;
	// }
	//
	// public Set<Carrera> getCarreras() {
	// return carreras;
	// }
	//
	// public void setCarreras(Set<Carrera> carreras) {
	// this.carreras = carreras;
	// }
	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof Campeonato)) {
			return false;
		}
		Campeonato other = (Campeonato) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Campeonato[ " + id + ", " + nombre + " ]";
	}

}
