/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.athagon.mgf1.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Athagon_German
 */
@Entity
@Table(name = "carrera")
@NamedQueries({
    @NamedQuery(name = "Carrera.findAll", query = "SELECT c FROM Carrera c"),
    @NamedQuery(name = "Carrera.findById", query = "SELECT c FROM Carrera c WHERE c.id = :id"),
    @NamedQuery(name = "Carrera.findByNumero", query = "SELECT c FROM Carrera c WHERE c.numero = :numero"),
    @NamedQuery(name = "Carrera.findByFechaInicio", query = "SELECT c FROM Carrera c WHERE c.fechaInicio = :fechaInicio"),
    @NamedQuery(name = "Carrera.findByFechaFin", query = "SELECT c FROM Carrera c WHERE c.fechaFin = :fechaFin"),
    @NamedQuery(name = "Carrera.findByTimestamp", query = "SELECT c FROM Carrera c WHERE c.timestamp = :timestamp")})
public class Carrera implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "numero")
    private int numero;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_inicio")
    @Temporal(TemporalType.DATE)
    private Date fechaInicio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_fin")
    @Temporal(TemporalType.DATE)
    private Date fechaFin;
    @Basic(optional = false)
    @Column(name = "timestamp",  nullable = false, updatable = false, insertable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")    
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;
    /*@OneToMany(mappedBy = "carrera")
    private Set<EnvioEmail> envioEmails;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "carrera1")
    private Set<LogroCarrera> logrosCarreras;*/
    @JoinColumn(name = "campeonato", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Campeonato campeonato;

    public Carrera() {
    }

    public Carrera(Integer id) {
        this.id = id;
    }

    public Carrera(Integer id, int numero, Date fechaInicio, Date fechaFin, Date timestamp) {
        this.id = id;
        this.numero = numero;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.timestamp = timestamp;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

/*    public Set<EnvioEmail> getEnvioEmails() {
        return envioEmails;
    }

    public void setEnvioEmails(Set<EnvioEmail> envioEmails) {
        this.envioEmails = envioEmails;
    }

    public Set<LogroCarrera> getLogroCarreras() {
        return logroCarreras;
    }

    public void setLogroCarreras(Set<LogroCarrera> logroCarreras) {
        this.logroCarreras = logroCarrers;
    }*/

    public Campeonato getCampeonato() {
        return campeonato;
    }

    public void setCampeonato(Campeonato campeonato) {
        this.campeonato = campeonato;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Carrera)) {
            return false;
        }
        Carrera other = (Carrera) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Carrera[ " + id + ", num=" + numero + " ]";
    }
    
}
