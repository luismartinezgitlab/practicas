/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.athagon.mgf1.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.springframework.beans.propertyeditors.CustomBooleanEditor;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 *
 * @author German Caballero
 */
@Entity
@Table(name = "clasificacion_carrera")
@NamedQueries({
    @NamedQuery(name = "ClasificacionCarrera.findAll", query = "SELECT c FROM ClasificacionCarrera c")})
public class ClasificacionCarrera implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    @Id
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "puesto")
    private Integer puesto;
    @Transient
    private int puestoCampeonato;
    @Basic(optional = false)
    @NotNull
    @Column(name = "campeonato")
    private int campeonato;
    @Basic(optional = false)
    @NotNull
    @Column(name = "carrera")
    private int carrera;
    @Basic(optional = false)
    @NotNull
    @Column(name = "numero_carrera")
    private int numeroCarrera;
    @Basic(optional = false)
    @NotNull
    @Column(name = "carrera_fecha_fin")
    @Temporal(TemporalType.DATE)
    private Date carreraFechaFin;
    @Basic(optional = false)
    @NotNull
    @Column(name = "piloto")
    private Integer piloto;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "apellidos")
    private String apellidos;
    @Column(name = "cargo")
    private String cargo;
    @Column(name = "logro")
    private int logro;
    @Transient
    private int logroAcumulado;
    @Column(name = "puntos_extra")
    private int puntosExtra;
    @Column(name = "numero_aleatorio")
    @JsonIgnore
    private float numeroAleatorio;
    @Column(name = "fecha_primer_cambio")
    @Temporal(TemporalType.DATE)
    private Date fechaPrimerCambio;
    @Column(name = "objetivo_camp")
    private int objetivoCamp;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "objetivo_carr")
    private float objetivoCarr;
    @Column(name = "desviacion")
    private float desviacion;
    @Column(name = "cobertura")
    private float cobertura;
    @Transient
    private float desviacionAcumulada;
    @Transient
    private float coberturaAcumulada;
    @Transient
    private int puntosCarrera;
    @Transient
    private int puntosCampeonato;
    @Transient
    private int posicionPorCobertura;
    @Transient
    private int puntosPorCobertura;
    @Transient
    private int puntosPorPosicion;
    @Basic(optional = false)
    @Column(name = "timestamp",  nullable = false, updatable = false, insertable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;

    public ClasificacionCarrera() {
    }

    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPuesto() {
        return puesto;
    }

    public void setPuesto(Integer puesto) {
        this.puesto = puesto;
    }

	public void setPuntosCampeonato(int puntosCampeonato) {
		this.puntosCampeonato = puntosCampeonato;
	}

	public int getPuestoCampeonato() {
		return puestoCampeonato;
	}
    public int getCampeonato() {
        return campeonato;
    }

    public void setCampeonato(int campeonato) {
        this.campeonato = campeonato;
    }

    public int getCarrera() {
        return carrera;
    }

    public void setCarrera(int carrera) {
        this.carrera = carrera;
    }
    public int getNumeroCarrera() {
        return numeroCarrera;
    }
    public void setNumeroCarrera(int numCarrera) {
        this.numeroCarrera = numCarrera;
    }

    public Date getCarreraFechaFin() {
        return carreraFechaFin;
    }

    public void setCarreraFechaFin(Date fechaFin) {
        this.carreraFechaFin = fechaFin;
    }
    public Integer getPiloto() {
        return piloto;
    }

    public void setPiloto(Integer piloto) {
        this.piloto = piloto;
    }
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

    public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}
	public int getLogro() {
        return logro;
    }

    public int getPuntosExtra() {
		return puntosExtra;
	}

	public void setPuntosExtra(int puntosExtra) {
		this.puntosExtra = puntosExtra;
	}

	public void setLogro(int logro) {
        this.logro = logro;
    }

    public float getCoberturaAcumulada() {
		return coberturaAcumulada;
	}

	public void setCoberturaAcumulada(float coberturaAcumulada) {
		this.coberturaAcumulada = coberturaAcumulada;
	}

	public int getLogroAcumulado() {
		return logroAcumulado;
	}

	public void setLogroAcumulado(int logroAcumulado) {
		this.logroAcumulado = logroAcumulado;
	}

	public float getNumeroAleatorio() {
		return numeroAleatorio;
	}

	public void setNumeroAleatorio(float numeroAleatorio) {
		this.numeroAleatorio = numeroAleatorio;
	}
    public Date getFechaPrimerCambio() {
        return fechaPrimerCambio;
    }
    public void setFechaPrimerCambio(Date fechaPrimerCambio) {
        this.fechaPrimerCambio = fechaPrimerCambio;
    }

	public int getObjetivoCamp() {
        return objetivoCamp;
    }

    public void setObjetivoCamp(int objetivoCamp) {
        this.objetivoCamp = objetivoCamp;
    }

    public float getObjetivoCarr() {
        return objetivoCarr;
    }

    public void setObjetivoCarr(float objetivoCarr) {
        this.objetivoCarr = objetivoCarr;
    }

    public float getDesviacion() {
        return desviacion;
    }

    public void setDesviacion(float desvicacion) {
        this.desviacion = desvicacion;
    }

    public float getDesviacionAcumulada() {
		return desviacionAcumulada;
	}

	public void setDesviacionAcumulada(float desviacionAcumulada) {
		this.desviacionAcumulada = desviacionAcumulada;
	}
    public float getCobertura() {
        return cobertura;
    }

    public void setCobertura(float cobertura) {
        this.cobertura = cobertura;
    }

    public int getPosicionPorCobertura() {
		return posicionPorCobertura;
	}

	public void setPosicionPorCobertura(int posicionPorCobertura) {
		this.posicionPorCobertura = posicionPorCobertura;
	}

	public int getPuntosPorCobertura() {
		return puntosPorCobertura;
	}

	public void setPuntosPorCobertura(int puntosPorCobertura) {
		this.puntosPorCobertura = puntosPorCobertura;
	}
	

	public int getPuntosPorPosicion() {
		return puntosPorPosicion;
	}

	public void setPuntosPorPosicion(int puntosPorPosicion) {
		this.puntosPorPosicion = puntosPorPosicion;
	}

	public int getPuntosCarrera() {
		return puntosCarrera;
	}

	public void setPuntosCarrera(int puntos) {
		this.puntosCarrera = puntos;
	}

	public int getPuntosCampeonato() {
		return puntosCampeonato;
	}

	public void setPuestoCampeonato(int puestoCampeonato) {
		this.puestoCampeonato = puestoCampeonato;
	}

	@Override
    public int hashCode() {
        int hash = super.hashCode();
        //hash += (campeonato != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClasificacionCarrera)) {
            return false;
        }
        ClasificacionCarrera other = (ClasificacionCarrera) object;
        if ((this.logro == 0 && other.logro != 0) || (this.logro != 0 && ! (this.logro== other.logro))) {
            return false;
        }
        return true;
    }
    public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	@Override
    public String toString() {
        return " Clasif Carr [campeonato "+ campeonato + ", carrera = " + carrera + ", piloto" + piloto + ", puesto " + puesto +", logro = " + logro + ", cobertura " + cobertura + ", puntosCampeonato " + puntosCampeonato + ", puntosCarrera " + puntosCarrera + " ]\n";
    }
	/*
    public ClasificacionCarrera createCopy() {
    	ClasificacionCarrera copia = new ClasificacionCarrera();
    	
    	copia.setCampeonato(this.campeonato);
    	copia.setCarrera(this.carrera);
    	copia.setCobertura(cobertura);
    	copia.setDesviacion(desviacion);
    	copia.logro = logro;
    	copia.objetivoCamp = objetivoCamp;
    	copia.objetivoCarr = objetivoCarr;
    	copia.piloto = piloto;
    	return copia;
    }
    */
}
