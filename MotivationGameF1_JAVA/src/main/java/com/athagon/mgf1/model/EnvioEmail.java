/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.athagon.mgf1.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.athagon.mgf1.enums.EstadoMail;
import com.athagon.mgf1.enums.TipoEmail;

/**
 *
 * @author Athagon_German
 */
@Entity
@Table(name = "envio_email")
@NamedQueries({ @NamedQuery(name = "EnvioEmail.findAll", query = "SELECT e FROM EnvioEmail e"),
		@NamedQuery(name = "EnvioEmail.findById", query = "SELECT e FROM EnvioEmail e WHERE e.id = :id"),
		@NamedQuery(name = "EnvioEmail.findByFechaHoraEnvio", query = "SELECT e FROM EnvioEmail e WHERE e.fechaHoraEnvio = :fechaHoraEnvio"),
		@NamedQuery(name = "EnvioEmail.findByTimestamp", query = "SELECT e FROM EnvioEmail e WHERE e.timestamp = :timestamp") })
public class EnvioEmail implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Basic(optional = false)
	@Column(name = "id")
	private Integer id;
	@Basic(optional = false)
	@NotNull
	@Column(name = "fecha_hora_envio")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaHoraEnvio;
	@Basic(optional = false)
	@Column(name = "timestamp", nullable = false, updatable = false, insertable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	@Temporal(TemporalType.TIMESTAMP)
	private Date timestamp;
	@JoinColumn(name = "campeonato", referencedColumnName = "id")
	@ManyToOne(optional = false)
	private Campeonato campeonato;
	@JoinColumn(name = "carrera", referencedColumnName = "id")
	@ManyToOne
	private Carrera carrera;
	/*
	 * @JoinColumn(name = "estado_mail", referencedColumnName = "id")
	 * 
	 * @ManyToOne(optional = false)
	 */

	@Enumerated(EnumType.STRING)
	private EstadoMail estadoMail;
	/*@JoinColumn(name = "tipo_email", referencedColumnName = "id")
	@ManyToOne(optional = false)*/
    @Enumerated(EnumType.STRING)
	private TipoEmail tipoEmail;

	public EnvioEmail() {
	}

	public EnvioEmail(Integer id) {
		this.id = id;
	}

	public EnvioEmail(Integer id, Date fechaHoraEnvio, Date timestamp) {
		this.id = id;
		this.fechaHoraEnvio = fechaHoraEnvio;
		this.timestamp = timestamp;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getFechaHoraEnvio() {
		return fechaHoraEnvio;
	}

	public void setFechaHoraEnvio(Date fechaHoraEnvio) {
		this.fechaHoraEnvio = fechaHoraEnvio;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public Campeonato getCampeonato() {
		return campeonato;
	}

	public void setCampeonato(Campeonato campeonato) {
		this.campeonato = campeonato;
	}

	public Carrera getCarrera() {
		return carrera;
	}

	public void setCarrera(Carrera carrera) {
		this.carrera = carrera;
	}

	public EstadoMail getEstadoMail() {
		return estadoMail;
	}

	public void setEstadoMail(EstadoMail estadoMail) {
		this.estadoMail = estadoMail;
	}

	public TipoEmail getTipoEmail() {
		return tipoEmail;
	}

	public void setTipoEmail(TipoEmail tipoEmail) {
		this.tipoEmail = tipoEmail;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof EnvioEmail)) {
			return false;
		}
		EnvioEmail other = (EnvioEmail) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "EnvioEmail[ " + id + ", " + estadoMail.toString() + " ]";
	}
}
