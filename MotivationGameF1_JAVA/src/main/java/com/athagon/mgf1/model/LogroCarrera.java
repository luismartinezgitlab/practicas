/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.athagon.mgf1.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 *
 * @author Athagon_German
 */
@Entity
@Table(name = "logro_carrera")
@NamedQueries({
    @NamedQuery(name = "LogroCarrera.findAll", query = "SELECT l FROM LogroCarrera l"),
    @NamedQuery(name = "LogroCarrera.findByCarrera", query = "SELECT l FROM LogroCarrera l WHERE l.logroCarreraPK.carrera = :carrera"),
    @NamedQuery(name = "LogroCarrera.findByPiloto", query = "SELECT l FROM LogroCarrera l WHERE l.logroCarreraPK.piloto = :piloto"),
    @NamedQuery(name = "LogroCarrera.findByLogro", query = "SELECT l FROM LogroCarrera l WHERE l.logro = :logro"),
    @NamedQuery(name = "LogroCarrera.findByTimestamp", query = "SELECT l FROM LogroCarrera l WHERE l.timestamp = :timestamp")})
public class LogroCarrera implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected LogroCarreraPK logroCarreraPK;
    @Column(name = "logro")
    private Integer logro;
    @Column(name = "puntos_extra")
    private Integer puntosExtra;
    @Column(name = "numero_aleatorio")
    @JsonIgnore
    private float numeroAleatorio;
    @Column(name = "fecha_primer_cambio")
    @Temporal(TemporalType.DATE)
    private Date fechaPrimerCambio;
    @Column(name = "fecha_ultimo_cambio")
    @Temporal(TemporalType.DATE)
    private Date fechaUltimoCambio;
    @Basic(optional = false)
    @Column(name = "timestamp",  nullable = false, insertable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;
    /*@OneToMany(cascade = CascadeType.ALL, mappedBy = "logroCarrera")
    private Set<Comentario> comentarios;*/
    @JoinColumn(name = "carrera", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Carrera carrera1;
    @JoinColumn(name = "piloto", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Persona persona;

    public LogroCarrera() {
    }

    public LogroCarrera(LogroCarreraPK logroCarreraPK) {
        this.logroCarreraPK = logroCarreraPK;
    }

    public LogroCarrera(LogroCarreraPK logroCarreraPK, Date timestamp) {
        this.logroCarreraPK = logroCarreraPK;
        this.timestamp = timestamp;
    }

    public LogroCarrera(int carrera, int piloto) {
        this.logroCarreraPK = new LogroCarreraPK(carrera, piloto);
    }

    public LogroCarreraPK getLogroCarreraPK() {
        return logroCarreraPK;
    }

    public void setLogroCarreraPK(LogroCarreraPK logroCarreraPK) {
        this.logroCarreraPK = logroCarreraPK;
    }

    public Integer getLogro() {
        return logro;
    }

    public void setLogro(Integer logro) {
        this.logro = logro;
    }

	public Integer getPuntosExtra() {
		return puntosExtra;
	}

	public void setPuntosExtra(Integer puntosExtra) {
		this.puntosExtra = puntosExtra;
	}

    
    public float getNumeroAleatorio() {
		return numeroAleatorio;
	}

	public void setNumeroAleatorio(float numeroAleatorio) {
		this.numeroAleatorio = numeroAleatorio;
	}
    public Date getFechaPrimerCambio() {
        return fechaPrimerCambio;
    }
    public void setFechaPrimerCambio(Date fechaPrimerCambio) {
        this.fechaPrimerCambio = fechaPrimerCambio;
    }
    public Date getFechaUltimoCambio() {
        return fechaUltimoCambio;
    }
    public void setFechaUltimoCambio(Date fechaUltimoCambio) {
        this.fechaUltimoCambio = fechaUltimoCambio;
    }
	public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

   /* public Set<Comentario> getComentarios() {
        return comentarios;
    }

    public void setComentarios(Set<Comentario> comentarios) {
        this.comentarios = comentarios;
    }*/

    public Carrera getCarrera1() {
        return carrera1;
    }

    public void setCarrera1(Carrera carrera1) {
        this.carrera1 = carrera1;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (logroCarreraPK != null ? logroCarreraPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LogroCarrera)) {
            return false;
        }
        LogroCarrera other = (LogroCarrera) object;
        if ((this.logroCarreraPK == null && other.logroCarreraPK != null) || (this.logroCarreraPK != null && !this.logroCarreraPK.equals(other.logroCarreraPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "LogroCarrera[ " + logroCarreraPK + ", '" + logro + "' ]";
    }
    
}
