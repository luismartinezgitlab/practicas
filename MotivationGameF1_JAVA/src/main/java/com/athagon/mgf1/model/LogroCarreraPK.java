/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.athagon.mgf1.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Athagon_German
 */
@Embeddable
public class LogroCarreraPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "carrera")
    private int carrera;
    @Basic(optional = false)
    @NotNull
    @Column(name = "piloto")
    private int piloto;

    public LogroCarreraPK() {
    }

    public LogroCarreraPK(int carrera, int piloto) {
        this.carrera = carrera;
        this.piloto = piloto;
    }

    public int getCarrera() {
        return carrera;
    }

    public void setCarrera(int carrera) {
        this.carrera = carrera;
    }

    public int getPiloto() {
        return piloto;
    }

    public void setPiloto(int piloto) {
        this.piloto = piloto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) carrera;
        hash += (int) piloto;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LogroCarreraPK)) {
            return false;
        }
        LogroCarreraPK other = (LogroCarreraPK) object;
        if (this.carrera != other.carrera) {
            return false;
        }
        if (this.piloto != other.piloto) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "[ carr=" + carrera + ", pil=" + piloto + " ]";
    }
    
}
