/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.athagon.mgf1.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.athagon.mgf1.enums.TipoPersona;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 *
 * @author Athagon_German
 */
@Entity
@Table(name = "participacion")
@NamedQueries({
    @NamedQuery(name = "Participacion.findAll", query = "SELECT o FROM Participacion o"),
    // @NamedQuery(name = "Participacion.findByCampeonato", query = "SELECT o FROM Participacion o WHERE o.ParticipacionPK.campeonato = :campeonato"),
    //@NamedQuery(name = "Participacion.findByParticipante", query = "SELECT o FROM Participacion o WHERE o.ParticipacionPK.participante = :participante"),
    @NamedQuery(name = "Participacion.findByObjetivo", query = "SELECT o FROM Participacion o WHERE o.objetivo = :objetivo")})
public class Participacion implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ParticipacionPK participacionPK;

    /* @JoinColumn(name = "tipo", referencedColumnName = "id")
    @ManyToOne(optional = false) */
    @Enumerated(EnumType.STRING)
    private TipoPersona tipo;
    
    @Column(name = "objetivo")
    private float objetivo;
    	// Versión autogenerada
    @JoinColumn(name = "campeonato", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch=FetchType.LAZY)
     
    	// Versión de ALVARO
    // @Column(name="campeonato", insertable=false, updatable=false)
    @JsonBackReference(value="campeonato")    
    private Campeonato campeonato1; 
    
    @JoinColumn(name = "participante", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch=FetchType.EAGER)
    @JsonBackReference(value="participante")
    private Persona persona;

    public Participacion() {
    }

    public Participacion(ParticipacionPK participacionPK) {
        this.participacionPK = participacionPK;
    }

    public Participacion(int campeonato, int participante) {
        this.participacionPK = new ParticipacionPK(campeonato, participante);
    }

    public ParticipacionPK getParticipacionPK() {
        return participacionPK;
    }

    public void setParticipacionPK(ParticipacionPK participacionPK) {
        this.participacionPK = participacionPK;
    }
    public TipoPersona getTipo() {
        return tipo;
    }

    public void setTipo(TipoPersona tipo) {
        this.tipo = tipo;
    }
    
    public float getObjetivo() {
        return objetivo;
    }

    public void setObjetivo(float objetivo) {
        this.objetivo = objetivo;
    }
/**/
    public Campeonato getCampeonato1() {
        return campeonato1;
    }

    public void setCampeonato1(Campeonato campeonato1) {
        this.campeonato1 = campeonato1;
    } 

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

	/**/
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (participacionPK != null ? participacionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Participacion)) {
            return false;
        }
        Participacion other = (Participacion) object;
        if ((this.participacionPK == null && other.participacionPK != null) || (this.participacionPK != null && !this.participacionPK.equals(other.participacionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Participacion[ " + participacionPK + ", " + tipo.toString() + " ]";
    }
    
}
