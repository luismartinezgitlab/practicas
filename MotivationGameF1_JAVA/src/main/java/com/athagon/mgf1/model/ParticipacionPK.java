/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.athagon.mgf1.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Athagon_German
 */
@Embeddable
public class ParticipacionPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "campeonato")
    private int campeonato;
    @Basic(optional = false)
    @NotNull
    @Column(name = "participante")
    private int participante;

    public ParticipacionPK() {
    }

    public ParticipacionPK(int campeonato, int participante) {
        this.campeonato = campeonato;
        this.participante = participante;
    }

    public int getCampeonato() {
        return campeonato;
    }

    public void setCampeonato(int campeonato) {
        this.campeonato = campeonato;
    }

    public int getParticipante() {
        return participante;
    }

    public void setParticipante(int participante) {
        this.participante = participante;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) campeonato;
        hash += (int) participante;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ParticipacionPK)) {
            return false;
        }
        ParticipacionPK other = (ParticipacionPK) object;
        if (this.campeonato != other.campeonato) {
            return false;
        }
        if (this.participante != other.participante) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "[camp= " + campeonato + ", persona= " + participante + " ]";
    }
}
