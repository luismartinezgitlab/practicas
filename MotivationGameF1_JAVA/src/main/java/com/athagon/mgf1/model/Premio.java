/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.athagon.mgf1.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.athagon.mgf1.enums.TipoPremio;
import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 *
 * @author Athagon_German
 */
@Entity
//@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Premio.findAll", query = "SELECT p FROM Premio p"),
    @NamedQuery(name = "Premio.findByCampeonato", query = "SELECT p FROM Premio p WHERE p.premioPK.campeonato = :campeonato"),
    @NamedQuery(name = "Premio.findById", query = "SELECT p FROM Premio p WHERE p.premioPK.id = :id"),
    @NamedQuery(name = "Premio.findByNombre", query = "SELECT p FROM Premio p WHERE p.nombre = :nombre"),
    @NamedQuery(name = "Premio.findByDescripcion", query = "SELECT p FROM Premio p WHERE p.descripcion = :descripcion")})
public class Premio implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PremioPK premioPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    private String nombre;
    @Size(max = 160)
    private String descripcion;
    @Enumerated(EnumType.STRING)
    private TipoPremio tipo;
    @Basic(optional = false)
    @NotNull
    private int puesto;
    @JoinColumn(name = "campeonato", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch=FetchType.LAZY)
    @JsonBackReference(value="campeonato")
    private Campeonato campeonato1;

    public Premio() {
    }

    public Premio(PremioPK premioPK) {
        this.premioPK = premioPK;
    }

    public Premio(PremioPK premioPK, String nombre) {
        this.premioPK = premioPK;
        this.nombre = nombre;
    }

    public Premio(int campeonato, int id) {
        this.premioPK = new PremioPK(campeonato, id);
    }

    public PremioPK getPremioPK() {
        return premioPK;
    }

    public void setPremioPK(PremioPK premioPK) {
        this.premioPK = premioPK;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public TipoPremio getTipo() {
		return tipo;
	}

	public void setTipo(TipoPremio tipo) {
		this.tipo = tipo;
	}

	public int getPuesto() {
		return puesto;
	}

	public void setPuesto(int puesto) {
		this.puesto = puesto;
	}
    public Campeonato getCampeonato1() {
        return campeonato1;
    }

    public void setCampeonato1(Campeonato campeonato1) {
        this.campeonato1 = campeonato1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (premioPK != null ? premioPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Premio)) {
            return false;
        }
        Premio other = (Premio) object;
        if ((this.premioPK == null && other.premioPK != null) || (this.premioPK != null && !this.premioPK.equals(other.premioPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.athagon.mgf1.jpa2.Premio[ premioPK=" + premioPK + " ]";
    }
    
}
