/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.athagon.mgf1.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Athagon_German
 */
@Embeddable
public class PremioPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    private int campeonato;
    @Basic(optional = false)
    @NotNull
    private int id;

    public PremioPK() {
    }

    public PremioPK(int campeonato, int id) {
        this.campeonato = campeonato;
        this.id = id;
    }

    public int getCampeonato() {
        return campeonato;
    }

    public void setCampeonato(int campeonato) {
        this.campeonato = campeonato;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) campeonato;
        hash += (int) id;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PremioPK)) {
            return false;
        }
        PremioPK other = (PremioPK) object;
        if (this.campeonato != other.campeonato) {
            return false;
        }
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.athagon.mgf1.jpa2.PremioPK[ campeonato=" + campeonato + ", id=" + id + " ]";
    }
    
}
