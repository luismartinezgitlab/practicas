/**
 *
 * @author Athagon_German
 */
package com.athagon.mgf1.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "tipo_email")
@NamedQueries({
    @NamedQuery(name = "TipoEmail.findAll", query = "SELECT t FROM TipoEmail t"),
    @NamedQuery(name = "TipoEmail.findById", query = "SELECT t FROM TipoEmail t WHERE t.id = :id"),
    @NamedQuery(name = "TipoEmail.findBynombre", query = "SELECT t FROM TipoEmail t WHERE t.nombre = :nombre")})
public class TipoEmail implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Enumerated(EnumType.STRING)
    private com.athagon.mgf1.enums.TipoEmail id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "activo")
    private boolean activo;

    public TipoEmail() {
    }

    public TipoEmail(com.athagon.mgf1.enums.TipoEmail id) {
        this.id = id;
    }
    public com.athagon.mgf1.enums.TipoEmail getId() {
        return id;
    }

    public void setId(com.athagon.mgf1.enums.TipoEmail id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean getActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }
	@Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoEmail)) {
            return false;
        }
        TipoEmail other = (TipoEmail) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "TipoEmail[ id=" + id + " ]";
    }
    
}
/**/