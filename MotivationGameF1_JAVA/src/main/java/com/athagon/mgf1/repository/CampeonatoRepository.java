package com.athagon.mgf1.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.athagon.mgf1.model.Campeonato;

public interface CampeonatoRepository extends JpaRepository<Campeonato, Integer> {

	  // @Query(value = "SELECT id FROM campeonato WHERE campeonato.sponsor = ?1", nativeQuery = true)
	  // Integer findIdFormulaRatioBySponsor(Integer idSponsor);

	  //@Query(value = "SELECT * FROM campeonato WHERE campeonato.formula_ratio = ?1", nativeQuery = true)
	  //List<Campeonato> findBySponsorFRatio(Integer idFormulaRatio);
	  
	  //@Query(value = "SELECT c FROM campeonato c LEFT JOIN c.objetivosParticipantes  WHERE participacion.participante = ?1")
	  //@Query(value = "SELECT c FROM campeonato c JOIN c.objetivosParticipantes op  WHERE op.persona.id = ?1", nativeQuery = true)
	
	//TODO: Ordenar por fecha fin
	
	  @Query(value = "SELECT * FROM campeonato INNER JOIN participacion  WHERE campeonato.id=participacion.campeonato AND participacion.participante = ?1 ORDER BY campeonato.fecha_fin DESC", nativeQuery = true)
	  List<Campeonato> findByParticipante(Integer idParticipante);
	  
	  
}
