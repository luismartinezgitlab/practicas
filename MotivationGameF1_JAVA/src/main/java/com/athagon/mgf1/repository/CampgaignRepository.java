package com.athagon.mgf1.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.athagon.mgf1.model.Campaign;
import com.athagon.mgf1.model.Empresa;

public interface CampgaignRepository extends JpaRepository<Campaign, Integer> {

}
