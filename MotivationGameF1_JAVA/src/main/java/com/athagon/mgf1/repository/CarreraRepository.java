package com.athagon.mgf1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.athagon.mgf1.model.Carrera;

public interface CarreraRepository extends JpaRepository<Carrera, Integer> {

	  @Query(value = "SELECT * FROM carrera WHERE carrera.campeonato = ?1 AND CAST(NOW() AS DATE) BETWEEN carrera.fecha_inicio AND carrera.fecha_fin", nativeQuery = true)
	  Carrera findCarreraActual(Integer idCampeonato);

	  @Query(value = "SELECT * FROM carrera WHERE carrera.campeonato = ?1 AND CAST((NOW() - INTERVAL 7 DAY) AS DATE) BETWEEN carrera.fecha_inicio AND carrera.fecha_fin", nativeQuery = true)
	  Carrera findCarreraAnterior(Integer idCampeonato);

	  @Query(value = "SELECT * FROM carrera WHERE carrera.campeonato = ?1 ORDER BY carrera.numero DESC LIMIT 0,1", nativeQuery = true)
	  Carrera findUltimaCarrera(Integer idCampeonato);
	  
}
