package com.athagon.mgf1.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.athagon.mgf1.model.ClasificacionCarrera;

public interface ClasificacionRepository extends JpaRepository<ClasificacionCarrera, Integer> {
	  
	  @Query(value = "SELECT @id/*'*/:=/*'*/@id+1 AS id, 0 AS puesto, tb_clasificacion.* FROM (SELECT * FROM clasificacion_carrera JOIN (SELECT @id/*'*/:=/*'*/ 0) AS contador WHERE clasificacion_carrera.carrera = ?1)  AS tb_clasificacion" /* ORDER BY tb_clasificacion.cobertura"*/, nativeQuery = true)
	  List<ClasificacionCarrera> findClasificacionCarerraByCarrera(Integer idCarrera);
	  
	  @Query(value = "SELECT @id/*'*/:=/*'*/@id+1 AS id, 0 AS puesto, tb_clasificacion.* FROM (SELECT * FROM clasificacion_carrera JOIN (SELECT @id/*'*/:=/*'*/ 0) AS contador WHERE clasificacion_carrera.campeonato = ?1 AND clasificacion_carrera.carrera <= ?2)  AS tb_clasificacion ORDER BY tb_clasificacion.carrera" /* ORDER BY tb_clasificacion.cobertura"*/, nativeQuery = true)
	  List<ClasificacionCarrera> findClasificacionCarerrasByCampeonato(Integer idCampeonato, Integer carreraActual);
}
