package com.athagon.mgf1.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.athagon.mgf1.model.Carrera;
import com.athagon.mgf1.model.ClasificacionCarrera;
import com.athagon.mgf1.model.Comentario;

public interface ComentarioRepository extends JpaRepository<Comentario, Integer> {

	@Query(value = "SELECT * FROM `comentario` WHERE participacion_campeonato = ?1 ORDER BY timestamp" /* ORDER BY tb_clasificacion.cobertura"*/, nativeQuery = true)	
	List<Comentario> findComentariosByCampeonato(Integer idCampeonato);
	
	@Query(value = "SELECT COUNT(*) FROM `comentario` WHERE participacion_campeonato = ?1" /* ORDER BY tb_clasificacion.cobertura"*/, nativeQuery = true)	
	Integer contarComentariosByCampeonato(Integer idCampeonato);
}

