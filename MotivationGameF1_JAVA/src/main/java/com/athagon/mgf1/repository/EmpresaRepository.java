package com.athagon.mgf1.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.athagon.mgf1.model.Empresa;

public interface EmpresaRepository extends JpaRepository<Empresa, Integer> {

}