package com.athagon.mgf1.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.athagon.mgf1.enums.EstadoMail;
import com.athagon.mgf1.model.EnvioEmail;

public interface EnvioEmailRepository extends JpaRepository<EnvioEmail, Integer> {
	
	@Query(value="SELECT e.* FROM envio_email e, tipo_email t WHERE e.estado_mail = ?1 AND e.fecha_hora_envio <  CURRENT_TIMESTAMP AND e.timestamp < ?2 AND t.id = e.tipo_email AND t.activo=TRUE LIMIT ?3", nativeQuery = true)
	//@Query(value="SELECT * FROM envio_email e WHERE e.estado_mail = ?1 AND e.fecha_hora_envio <  CURRENT_TIMESTAMP AND e.timestamp < ?2 LIMIT ?3", nativeQuery = true)
	List<EnvioEmail> findByEstado(String estadoMail, Date timestamp, int cantidad);
		
	
	//@Query(value="SELECT * FROM envio_email e WHERE e.estado_mail = 'PRE' AND e.fecha_hora_envio <  CURRENT_TIMESTAMP AND e.timestamp < ?1 LIMIT ?2", nativeQuery = true)
	//List<EnvioEmail> findByEstado(Date timestamp, int cantidad);
}