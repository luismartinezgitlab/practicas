package com.athagon.mgf1.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.athagon.mgf1.model.LogroCarrera;
import com.athagon.mgf1.model.LogroCarreraPK;

public interface LogroCarreraRepository extends JpaRepository<LogroCarrera, LogroCarreraPK> {

//	  @Query("select fRatio from FormulaRatio fRatio where fRatio.sponsor = ?1")
//	  @Query(value = "SELECT id FROM formula_ratio where formula_ratio.sponsor = ?1", nativeQuery = true)
//	  Integer findIdFormulaRatioBySponsor(Integer idSponsor);

	  //@Query(value = "SELECT @id/*'*/:=/*'*/@id+1 AS id, 0 AS puesto, tb_clasificacion.* FROM (SELECT * FROM clasificacion_carrera JOIN (SELECT @id/*'*/:=/*'*/ 0) AS contador WHERE clasificacion_carrera.campeonato = ?1 AND clasificacion_carrera.carrera <= ?2)  AS tb_clasificacion" /* ORDER BY tb_clasificacion.cobertura"*/, nativeQuery = true)
	  //List<ClasificacionCarrera> findClasificacionCarerrasByCampeonato(Integer idCampeonato, Integer carreraActual);
	  
	  @Query(value = "SELECT logro_carrera.* FROM logro_carrera INNER JOIN carrera ON logro_carrera.carrera = carrera.id WHERE logro_carrera.piloto =?2 AND carrera.campeonato = ?1 AND carrera.fecha_inicio <= NOW() ORDER BY logro_carrera.carrera DESC", nativeQuery = true)
	  List<LogroCarrera> findLogrosCarerrasByCampeonato(Integer idCampeonato, Integer idParticipante);
}
