package com.athagon.mgf1.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.athagon.mgf1.model.Participacion;
import com.athagon.mgf1.model.ParticipacionPK;

public interface ParticipacionRepository extends JpaRepository<Participacion, ParticipacionPK> {

	  // @Query(value = "select op from objetivoParticipante op where formula_ratio.sponsor = ?1")
	  @Query(value="SELECT o FROM Participacion o WHERE o.participacionPK.campeonato = ?1")
	  List<Participacion> findByIdCampeonato(Integer idCampeonato);

	  @Query(value="SELECT o FROM Participacion o WHERE o.participacionPK.participante = ?1")
	  List<Participacion> findByIdParticipante(Integer idParticipante);
	  
	  @Query(value="SELECT o FROM Participacion o WHERE o.participacionPK.participante = ?1 AND o.participacionPK.campeonato = ?2")
	  List<Participacion> findByIdParticipanteYCampeonato(Integer idParticipante, Integer idCampeonato);
}
