package com.athagon.mgf1.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.athagon.mgf1.model.Persona;

public interface PersonaRepository extends JpaRepository<Persona, Integer> {

	  // @Query(value = "select op from objetivoParticipante op where formula_ratio.sponsor = ?1")
	  @Query(value="SELECT p FROM Persona p INNER JOIN p.participaciones o WHERE  o.participacionPK.campeonato = ?1")
	  List<Persona> findByIdCampeonato(Integer idCampeonato);
	  
	  @Query(value="SELECT p FROM Persona p WHERE  p.email = ?1")
	  List<Persona> findByEmail(String email);
}