package com.athagon.mgf1.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.athagon.mgf1.model.Participacion;
import com.athagon.mgf1.model.Premio;

public interface PremioRepository extends JpaRepository<Premio, Integer> {

	  // @Query(value = "select op from objetivoParticipante op where formula_ratio.sponsor = ?1")
	  @Query(value="SELECT p FROM Premio p WHERE p.premioPK.campeonato = ?1")
	  List<Premio> findByIdCampeonato(Integer idCampeonato);
	  
}
