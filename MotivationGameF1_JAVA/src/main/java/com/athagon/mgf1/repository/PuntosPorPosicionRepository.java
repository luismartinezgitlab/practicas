package com.athagon.mgf1.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.athagon.mgf1.model.PuntosPorPosicion;

public interface PuntosPorPosicionRepository  extends JpaRepository<PuntosPorPosicion, Integer> {

}
