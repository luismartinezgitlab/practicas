(function(angular) {
  var CampaignFactory = function($resource) {
    return $resource('/items/:id', {
      id: '@id'
    }, {
      update: {
        method: "PUT"
      },
      remove: {
        method: "DELETE"
      }
    });
  };
  CampaignFactory.$inject = ['$resource'];
  
  angular.module("f1App.services").factory("Campaign", CampaignFactory);
}(angular));