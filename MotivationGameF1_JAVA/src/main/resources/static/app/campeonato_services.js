(function(angular) {

  var CampeonatoFactory = function($resource) {
    return $resource('/campeonatos/:id',  // url 
    				{ id: '@id' }, 	// paramDefaults
    				{ update: {
				        method: "PUT"
				      },
				      remove: {
				        method: "DELETE"
				      }
				    }		// actions
				);
  };
  CampeonatoFactory.$inject = ['$resource'];
  
  var ListaParticipanteFactory = function($resource) {
	    return $resource('/personas/participantes/:id',  // url 
				{ id: '@id' }, 	// paramDefaults
				{ update: {
			        method: "PUT"
			      }
			    }		// actions
			);
  };

  ListaParticipanteFactory.$inject = ['$resource'];
  
  var ListaObjetivosFactory = function($resource) {
    return $resource('/objetivos/participantes/:id',  // url 
    				{ id: '@id' }, 	// paramDefaults
    				{ update: {
				        method: "PUT"
				      }
				    }		// actions
    				);
  };
  ListaObjetivosFactory.$inject = ['$resource'];  
  
  var ObjetivoFactory = function($resource) {
	    return $resource('/objetivos/:id',  // url 
	    				{ id: '@id' }, 	// paramDefaults
	    				{ update: {
					        method: "PUT"
					      }
					    }		// actions
	    				);
  };
  ObjetivoFactory.$inject = ['$resource'];  

  var ListaPremiosFactory = function($resource) {
    return $resource('/premios/:id',  // url 
    				{ id: '@id' }, 	// paramDefaults
    				{ update: {
				        method: "PUT"
				      }
				    }		// actions
				);
  };
  ListaPremiosFactory.$inject = ['$resource'];  
  
  var PremioFactory = function($resource) {
	    return $resource('/premios/:id',  // url 
	    				{ id: '@id' }, 	// paramDefaults
	    				{ update: {
					        method: "PUT"
					      }
					    }		// actions
	    				);
  };
  PremioFactory.$inject = ['$resource'];  
  
  angular.module("f1App.services").factory("CampeonatoF", CampeonatoFactory);
  angular.module("f1App.services").factory("ListaParticipanteF", ListaParticipanteFactory);
  angular.module("f1App.services").factory("ListaObjetivosF", ListaObjetivosFactory);
  angular.module("f1App.services").factory("ObjetivoF", ObjetivoFactory);
  angular.module("f1App.services").factory("ListaPremiosF", ListaPremiosFactory);
  angular.module("f1App.services").factory("PremioF", PremioFactory);
}(angular));