(function(angular) {

	var EmpresaController = function($scope, EmpresaF, PersonaF) {

		inicializarSesion($scope, EmpresaF, PersonaF);
		
		$scope.addSponsor = function(nombreSponsor, apellidosSponsor, emailSponsor, nombreEmpresa) {

			new EmpresaF({
			    'nombre': nombreEmpresa /*,
			    'cif': ""*/
			}).$save(
				function(empresa) {        
				  
					if (empresa.id > 0) {
						new PersonaF({
					        'nombre': nombreSponsor,
					        'apellidos': apellidosSponsor,
					        'email': emailSponsor,
					        'empresa': {
					        	'id' : empresa.id,
							    'nombre': nombreEmpresa,
							    'cif': ""
							  }
					        }).$save(function(persona) {
					        	sessionStorage.idSponsor =  persona.id;
					        	sessionStorage.idEmpresa =  empresa.id;
					        	/*sessionStorage.persona = persona;
					        	sessionStorage.empresa = empresa; */ 
	
					        	$scope.newEmpresa = "";     
					    		$scope.newNombreSponsor = "";
					    		$scope.newApellidosSponsor = "";
					    		$scope.newEmailSponsor = ""; 
					    		
								window.location='campeonato.html';
					        });
					} else {
						alert("No se ha podido dar de alta la empresa");
					}
			  });
    };
  };
  EmpresaController.$inject = ['$scope', 'EmpresaF', 'PersonaF'];
  
  angular.module("f1App.controllers").controller("EmpresaController", EmpresaController);
  
}(angular));