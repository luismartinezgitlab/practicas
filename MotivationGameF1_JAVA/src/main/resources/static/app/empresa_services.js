(function(angular) {
  var EmpresaFactory = function($resource) {
    return $resource('/empresas/:id',  // url 
    				{ id: '@id' }, 	// paramDefaults
    				{ update: {
				        method: "PUT"
				      },
				      remove: {
				        method: "DELETE"
				      }
				    }		// actions
    			);
  };
  EmpresaFactory.$inject = ['$resource'];

  var PersonaFactory = function($resource) {
    return $resource('/personas/:id',  // url 
    				{ id: '@id' }, 	// paramDefaults
    				{ update: {
				        method: "PUT"
				      },
				      remove: {
				        method: "DELETE"
				      }
				    }		// actions
    				);
  };
  PersonaFactory.$inject = ['$resource'];
   
  angular.module("f1App.services").factory("EmpresaF", EmpresaFactory);
  angular.module("f1App.services").factory("PersonaF", PersonaFactory);
}(angular));