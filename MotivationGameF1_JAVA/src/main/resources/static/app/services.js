(function(angular) {
  var CampaignFactory = function($resource) {
    return $resource('/items/:id', {
      id: '@id'
    }, {
      update: {
        method: "PUT"
      },
      remove: {
        method: "DELETE"
      }
    });
  };
  CampaignFactory.$inject = ['$resource'];

  var EmpresaFactory = function($resource) {
    return $resource('/empresas/:id',  // url 
    				{ id: '@id' }, 	// paramDefaults
    				{ update: {
				        method: "PUT"
				      },
				      remove: {
				        method: "DELETE"
				      }
				    }		// actions
    				);
  };
  EmpresaFactory.$inject = ['$resource'];

  var PersonaFactory = function($resource) {
    return $resource('/personas/:id',  // url 
    				{ id: '@id' }, 	// paramDefaults
    				{ update: {
				        method: "PUT"
				      },
				      remove: {
				        method: "DELETE"
				      }
				    }		// actions
    				);
  };
  PersonaFactory.$inject = ['$resource'];
  

  var CampeonatoFactory = function($resource) {
    return $resource('/campeonatos/:id',  // url 
    				{ id: '@id' }, 	// paramDefaults
    				{ update: {
				        method: "PUT"
				      },
				      remove: {
				        method: "DELETE"
				      }
				    }		// actions
    				);
  };
  CampeonatoFactory.$inject = ['$resource'];
  

  var ObjetivoParticipanteFactory = function($resource) {
    return $resource('/objetivos/:id',  // url 
    				{ id: '@id' }, 	// paramDefaults
    				{ update: {
				        method: "PUT"
				      },
				      remove: {
				        method: "DELETE"
				      }
				    }		// actions
    				);
  };
  ObjetivoParticipanteFactory.$inject = ['$resource'];
  
  
  angular.module("f1App.services").factory("Campaign", CampaignFactory);
  angular.module("f1App.services").factory("EmpresaF", EmpresaFactory);
  angular.module("f1App.services").factory("PersonaF", PersonaFactory);
  angular.module("f1App.services").factory("CampeonatoF", CampeonatoFactory);
  angular.module("f1App.services").factory("ObjetivoParticipanteF", ObjetivoParticipanteFactory);
}(angular));