
function inicializarSesion($scope, EmpresaF, PersonaF) {
	
	$scope.sponsor = undefined;
	$scope.empresa = undefined;
	
	$scope.newEmpresa = "";     
	$scope.newNombreSponsor = "";
	$scope.newApellidosSponsor = "";
	$scope.newEmailSponsor = "";

	$scope.noEstaRegistrado = function () {
		$scope.empresa = undefined;
		$scope.sponsor = undefined;
		sessionStorage.idSponsor =  "";
		sessionStorage.idEmpresa =  "";
			
		if ((window.location+"").search("registro.html") < 0) {
			window.location = "registro.html";								
		}		
		$scope.datosCargados = true;
	}
	if (sessionStorage.idSponsor > 0) {
		  
		PersonaF.get( { 'id': sessionStorage.idSponsor }, function(response) {
			
			$scope.sponsor = response ? response : [];

			if (typeof($scope.sponsor) != "undefined" && $scope.sponsor.id > 0) {
				
				EmpresaF.get( { 'id': sessionStorage.idEmpresa }, function(response) {

					$scope.empresa = response ? response : [];
					if ($scope.empresa.id == 0 || $scope.empresa.id != $scope.sponsor.empresa.id) {
						
						$scope.noEstaRegistrado();						
					} else { // Todo ha ido bien
						$scope.datosCargados = true;
					}
				});
			} else { 
				$scope.noEstaRegistrado();				
			}
		});
	} else { 
		$scope.noEstaRegistrado();				
	}
    $scope.haySesion = function() {
	   	 var siHaySesion = sessionStorage.idSponsor >  0;
	   	 return  siHaySesion;
    }
}

 function limpiarDatos() {
 	sessionStorage.idSponsor =  "";
	sessionStorage.idEmpresa =  '';
	window.location='registro.html';
 }
function localSetItem(campo, valor) {
	if (window.localStorage) {	 
	  localStorage.setItem(campo, valor);	  
	  return valor;
	}
	else {
	  throw new Error('Tu Browser no soporta LocalStorage!');
	}
}
function localGetItem(campo) {
	if (window.localStorage) {	 	 
	  var valor = localStorage.getItem("nombre");	   
	  return valor;
	}
	else {
	  throw new Error('Tu Browser no soporta LocalStorage!');
	}
}
function localRemoveItem(campo, valor) {
	if (window.localStorage) {	   
	  localStorage.removeItem(campo);
	}
	else {
	  throw new Error('Tu Browser no soporta LocalStorage!');
	}
}
function sessionSetItem(campo, valor) {
	if (window.sessionStorage) {	 
		sessionStorage.setItem(campo, valor);	  
	  return valor;
	}
	else {
	  throw new Error('Tu Browser no soporta LocalStorage!');
	}
}
function sessionGetItem(campo) {
	if (window.sessionStorage) {	 	 
	  var valor = sessionStorage.getItem("nombre");	   
	  return valor;
	}
	else {
	  throw new Error('Tu Browser no soporta LocalStorage!');
	}
}
function sessionRemoveItem(campo, valor) {
	if (window.sessionStorage) {	   
	  sessionStorage.removeItem(campo);
	}
	else {
	  throw new Error('Tu Browser no soporta LocalStorage!');
	}
}
