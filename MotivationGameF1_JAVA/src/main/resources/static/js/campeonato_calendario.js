/**
 * 
 */

var numDias = 0;

var startDate;
var endDate;

// Esta variable se puede cambiar a false para que sea el domingo 
var lunesPrimerDia = true;

var thisDatePicker;
var instSettings;

var alSeleccionar;

$(function() {
	$.datepicker.regional['es'] = {
			 closeText: 'Cerrar',
			 prevText: '<Ant',
			 nextText: 'Sig>',
			 currentText: 'Hoy',
			 monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
			 monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
			 dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
			 dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
			 dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
			 weekHeader: 'Sm',
			 dateFormat: 'dd/mm/yy',
			 firstDay: 1,
			 isRTL: false,
			 showMonthAfterYear: false,
			 yearSuffix: ''
	 };
	$.datepicker.setDefaults( $.datepicker.regional[ "es" ] );
	
    var selectCurrentWeek = function() {
        window.setTimeout(function () {
            $('.week-picker').find('.ui-datepicker-current-day a').addClass('ui-state-active')
        }, 1);
    }
    
    alSeleccionar = function(scope) {
    	
        // Numero de dias seleccionados en el calendario
        
    	if (typeof(thisDatePicker) != "undefined") {
        	
    		var date = $(thisDatePicker).datepicker('getDate');
    		
            startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + (lunesPrimerDia ? 1 : 0));
    		endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + numDias + (lunesPrimerDia ? 1 : 0));
    		
    		//TODO: formato inglés
            // var dateFormat = inst.settings.dateFormat || $.datepicker._defaults.dateFormat;
    		var dateFormat = "DD, d MM, yy";

            $('#startDate').text($.datepicker.formatDate( dateFormat, startDate, instSettings ));
            $('#endDate').text($.datepicker.formatDate( dateFormat, endDate, instSettings ));
            
            selectCurrentWeek();

            if (typeof(scope) == "undefined") {
                var scope = angular.element($("body")).scope();
                scope.$apply(function(){
                	scope.fechaSeleccionada = true;
                	scope.fechaInicio = startDate;
                	scope.fechaFin = endDate;
                });            	
            } else {
            	scope.fechaSeleccionada = true;
            	scope.fechaInicio = startDate;
            	scope.fechaFin = endDate;
            }    		
    	}
    };
    $('.week-picker').datepicker( {
        showOtherMonths: true,
        selectOtherMonths: true,
        firstDay: lunesPrimerDia ? 1 : 0,
		minDate: (1- (new Date()).getDay()),
		defaultDate: 0,
        onSelect:  function(dateText, inst) {
        	thisDatePicker = this;
        	instSettings = inst;
        	alSeleccionar();
        },
        beforeShowDay: function(date) {
            var cssClass = '';
            if(date >= startDate && date <= endDate)
                cssClass = 'ui-datepicker-current-day';
            return [true, cssClass];
        },
        onChangeMonthYear: function(year, month, inst) {
            selectCurrentWeek();
        }
    });    
    $('.week-picker .ui-datepicker-calendar tr').live('mousemove', function() { $(this).find('td a').addClass('ui-state-hover'); });
    $('.week-picker .ui-datepicker-calendar tr').live('mouseleave', function() { $(this).find('td a').removeClass('ui-state-hover'); });
});