#!/bin/bash

cd ..
nombreJar=$(ls mgm1*.jar)

rm -r /var/athagon/mgm1
#mkdir /var/athagon/mgm1
mv ../target 	/var/athagon/mgm1
mv /var/athagon/mgm1/classes/scripts /var/athagon/mgm1/bin
ln -s /var/athagon/mgm1/$nombreJar  /var/athagon/mgm1/bin/mgm1.jar
chmod ug+x /var/athagon/mgm1/bin/*.sh
chmod ug+x /var/athagon/mgm1/bin/mgm1srv

rm /etc/init.d/mgm1srv
update-rc.d -f mgm1srv remove

ln -s /var/athagon/mgm1/bin/mgm1srv /etc/init.d/mgm1srv
update-rc.d mgm1srv start 99 3 4 5 . stop 10 0 1 2 6 .

rm /var/athagon/mgm1/classes/mgm1-install.sh
