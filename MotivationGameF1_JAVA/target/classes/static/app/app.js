(function(angular) {
  angular.module("f1App.controllers", []);
  angular.module("f1App.services", []);
  angular.module("f1App", ["ngResource", "f1App.controllers", "f1App.services"]);
} (angular));
