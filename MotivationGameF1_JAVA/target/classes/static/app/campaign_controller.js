// $scope.campaigns = Formulario de campañas

(function(angular) {

  var AppController = function($scope, Campaign) {
     Campaign.query(function(response) {
      $scope.campaigns = response ? response : [];
    });
    
    $scope.addCampaign = function(description) {
      new Campaign({
        description: description,
        checked: false
      }).$save(function(c) {
        $scope.campaigns.push(c);
      });
      $scope.newCampaign = "";
    };
    
    $scope.updateCampaign = function(c) {
      c.$update();
    };
    
    $scope.deleteCampaign = function(c) {
      c.$remove(function() {
        $scope.campaigns.splice($scope.campaigns.indexOf(c), 1);
      });
    };
  };

  AppController.$inject = ['$scope', 'Campaign'];

  //Then we add the controller's constructor function to the module using the .controller() method. 
  angular.module("f1App.controllers").controller("AppController", AppController);
}(angular));