
(function(angular) {

	var CampeonatoController = function($scope, EmpresaF, PersonaF, CampeonatoF, ListaParticipanteF, ListaObjetivosF, ObjetivoF, ListaPremiosF, PremioF ) {

		$scope.acumuladoObjetivo = 0;
		
		$scope.inicializarDatosCampeonato = function() {

			$scope.numPaso = 1; // Paso por defecto

			$scope.newNombreCampeonato = "";
			$scope.newTipoObjetivo = "";
			$scope.newNumeroCarreras = 1;

		    $scope.setNumDias($scope.newNumeroCarreras);

		    $scope.pilotos = new Array();
		    $scope.espectadores = new Array();
		    
		    $scope.premios = new Array();
		    $scope.premios[0] = {'titulo': '1º Campeonato', 'placeholder': 'Primer premio campeonato', 'required': true, 'premioPK' : { 'campeonato': 0, 'id': 1}, 'tipo': 'CAMP', 'puesto': 1 };
		    $scope.premios[1] = {'titulo': '2º Campeonato', 'placeholder': 'Segundo premio campeonato', 'required': false, 'premioPK' : { 'campeonato': 0, 'id': 2}, 'tipo': 'CAMP', 'puesto': 2 };
		    $scope.premios[2] = {'titulo': '3º Campeonato', 'placeholder': 'Tercer premio campeonato', 'required': false, 'premioPK' : { 'campeonato': 0, 'id': 3}, 'tipo': 'CAMP', 'puesto': 3 };
		    $scope.premios[3] = {'titulo': '1º Carrera', 'placeholder': 'Primer premio carrera', 'required': false, 'premioPK' : { 'campeonato': 0, 'id': 4}, 'tipo': 'CARR', 'puesto': 1 };
		    $scope.premios[4] = {'titulo': '2º Carrera', 'placeholder': 'Segundo premio carrera', 'required': false, 'premioPK' : { 'campeonato': 0, 'id': 5}, 'tipo': 'CARR', 'puesto': 2 };
		    $scope.premios[5] = {'titulo': '3º Carrera', 'placeholder': 'Tercer premio carrera', 'required': false, 'premioPK' : { 'campeonato': 0, 'id': 6}, 'tipo': 'CARR', 'puesto': 3 };

		}
		$scope.sincroFechas = function() {
			if (Object.prototype.toString.call(startDate) === '[object Date]') {

            	$scope.fechaInicio = startDate;
            	$scope.fechaFin = endDate;
			} else {

				startDate = $scope.fechaInicio;
				endDate = $scope.fechaFin;
			}
		}
		$scope.getAcumuladoObjetivo = function() {

			var total = 0;
		    for(var i = 0; i < $scope.pilotos.length; i++) {

		    	var nObjetivo = parseInt($scope.pilotos[i].objetivo); 
		    	if ( !isNaN(nObjetivo)) {
		    		total += nObjetivo;
		    	} else {
		    		$scope.pilotos[i].objetivo = 0;
		    	}
		    }
		    return total;
		}
	    // Las fechas las cojemos de las variables globales. El resto del formulario
	    $scope.enviarCampeonatoCompleto = function() {
	
			new CampeonatoF({
			    'nombre': $scope.newNombreCampeonato,
			    'tipoObjetivo': $scope.newTipoObjetivo,
			    'cantidadObjetivo' : $scope.getAcumuladoObjetivo(),
			    'numeroCarreras': $scope.newNumeroCarreras,
			    'fechaInicio': startDate,
			    'fechaFin': endDate,
			    'idSponsor' : sessionStorage.idSponsor
		        /*'sponsor': {
		        	'id' : sessionStorage.idSponsor
				  }*/
			  }).$save( function(campeonato) {
				  
					if (campeonato.id > 0) {
			        	$scope.enviarParticipantes( campeonato.id );
					} else {
						alert("No se ha podido dar de alta el campeonato");
			    	}
				});
	    };
	    $scope.addPiloto = function(nombre, apell, email, cuantiaObj) {
	    	$scope.addParticipante($scope.pilotos, nombre, apell, email, 'PIL', cuantiaObj);
	    	
	        $scope.newNombrePiloto			= "";
	        $scope.newApellidosPiloto		= "";
	        $scope.newEmailPiloto			= "";
	        $scope.newCuantiaObjetivoPiloto	= "";
	    }
	    $scope.addEspectador = function(nombre, apell, email) {
	    	$scope.addParticipante($scope.espectadores, nombre, apell, email, 'ESP', null);

	        $scope.newNombreEspectador			= "";
	        $scope.newApellidosEspectador		= "";
	        $scope.newEmailEspectador			= "";
	        $scope.newCuantiaObjetivoEspectador	= "";
        }
	    $scope.addParticipante = function(arrayParticipantes, nombre, apell, mail, tipo, cuantiaObj) {
	    	var participante = new PersonaF({
	    		// 'idTemporal': $scope.participantes.length + 1,
		        'nombre': nombre,
		        'apellidos': apell,
		        'email': mail,
		        'tipo': tipo,
				'objetivo': cuantiaObj,
		        'empresa': {
		        	'id' : sessionStorage.idEmpresa
				  }
		        });
	        // $scope.participantes.push(participante);
	    	arrayParticipantes.splice(0, 0, participante);
	    }
	    $scope.deleteParticipante = function(arrayParticipantes, participante) {
	    	arrayParticipantes.splice(arrayParticipantes.indexOf(participante), 1);	    	
	    } 
		$scope.enviarParticipantes = function(idCampeonato) {
			
			new ListaParticipanteF($scope.pilotos).$save(function(nuevaListaPilotos) {
				
				new ListaParticipanteF($scope.espectadores).$save(function(nuevaListaEspectadores) {
					
					$scope.objetivos = new Array();
					
					// Añadimos el Sponsor
					var objetivo = new ObjetivoF({
						"participacionPK":
							{"campeonato": idCampeonato,
							 "participante":sessionStorage.idSponsor},
							 "tipo": "SPN",
							 "objetivo": null
					});
					$scope.objetivos.push(objetivo);
										
					angular.forEach(nuevaListaPilotos, function(participante, key) {
						if (typeof(participante) != "undefined") {
							
							if (participante.id > 0) {
								
								var cuantiaObjetivoParticipante = null;
								// Buscamos objetivos entre los guardados en $scope

								angular.forEach($scope.pilotos, function(participanteScope, key) {
									if (participanteScope.email == participante.email ) {
										
										cuantiaObjetivoParticipante = participanteScope.objetivo;
									}
								});
								var objetivo = new ObjetivoF({
									"participacionPK":
										{"campeonato": idCampeonato,
										 "participante":participante.id},
										 "tipo": "PIL",
										 "objetivo":cuantiaObjetivoParticipante
								});
								$scope.objetivos.push(objetivo);
							}
					        
						}
					});
					angular.forEach(nuevaListaEspectadores, function(participante, key) {
						if (typeof(participante) != "undefined") {
							if (participante.id > 0) {
								var objetivo = new ObjetivoF({
									"participacionPK":
										{"campeonato": idCampeonato,
										 "participante":participante.id},
										 "tipo": "ESP",
										 "objetivo": null
								});
								$scope.objetivos.push(objetivo);
							}
						}
					});
					new ListaObjetivosF($scope.objetivos).$save( function(ok) {
						 //else {
						//			    			alert("No se ha podido dar de alta los pilotos");
				    	//	}
						$scope.enviarPremios(idCampeonato);
					});	
				});				
			});
		}
		$scope.enviarPremios = function(idCampeonato) {

			var listaPremios = new Array();
			
			var posicion = 0;
			angular.forEach($scope.premios, function(premio, key) {
				
				if (premio.nombre != "" && typeof(premio.nombre) != "undefined") {
					listaPremios[posicion] = premio;
					listaPremios[posicion].premioPK.campeonato = idCampeonato;
					posicion++;
				}
			});
			
			new ListaPremiosF(listaPremios).$save(function(nuevaListaPremios) {
				
				alert("QUE COMIENCE EL JUEGO!!!! ");
				
				window.location = "";
			});
		}
	    $scope.setNumDias = function(semanas) {
			  numDias = semanas * 7 - 1;
			  if (numDias < 0)
				  numDias = 0;
			  if (typeof($) != "undefined") {
//				  if (typeof($.selectCurrentWeek) != "undefined") {
//					  $.selectCurrentWeek();
//				  }
				  alSeleccionar($scope);
		        window.setTimeout(function () {
		        	$(".week-picker").datepicker( "refresh" );
		            $('.week-picker').find('.ui-datepicker-current-day a').addClass('ui-state-active');
		            
		        }, 1);
			  }
		} 
	    
	    $scope.irAlPaso = function(nuevoPaso) {
	    	if (nuevoPaso != $scope.numPaso) {	    		
	    		if (nuevoPaso > $scope.numPaso) {
	    			if (document.formCampeonato[$scope.numPaso - 1].$valid) {
	    				$scope.numPaso=nuevoPaso;
	    			}
	    		} else {
	    			$scope.numPaso=nuevoPaso;
	    		}
	    	} 
	    }
	    
		/* 		INICIALIZACIÓN DE DATOS		*/
		
		inicializarSesion($scope, EmpresaF, PersonaF);
		
		$scope.inicializarDatosCampeonato();
		
		//TODO: QUITAR ESTO
		// sessionStorage.idCampeonato = 1;
		
		/*if (sessionStorage.idCampeonato > 0) {
			
			Campeonato.query(function(response) {
				$scope.pilotos = response ? response : [];
			});
		}*/
		$scope.participantes = [];
	}; 
		
	CampeonatoController.$inject = ['$scope', 'EmpresaF', 'PersonaF', 'CampeonatoF', 'ListaParticipanteF', 'ListaObjetivosF', 'ObjetivoF', 'ListaPremiosF', 'PremioF'];
	  
	angular.module("f1App.controllers").controller("CampeonatoController", CampeonatoController);
}(angular));